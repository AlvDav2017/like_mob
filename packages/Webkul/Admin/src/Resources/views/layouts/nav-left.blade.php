<div class="navbar-left aside-nav">
    <ul class="menubar">
        @foreach ($menu->items as $menuItem)
            <li class="menu-item {{ $menu->getActive($menuItem) }} menu-{{$menuItem['key']}}">
                @if (count($menuItem['children'])!= 0)
                    <a href="#" @click="toggleMenu('<?="menu-".$menuItem['key']?>')">
                        <span class="icon {{ $menuItem['icon-class'] }}"></span>
                        <span>{{ trans($menuItem['name']) }}</span>
                        <i class="arrow-down-icon {{ $menu->getActive($menuItem)?'rotate-180':'' }}"></i>
                    </a>
                    <?php $check=0; ?>
                    @foreach ($menuItem['children'] as $item)
                        @if ($item['url'] == Request::url())
                        <?php $check=1; ?>
                        @endif
                    @endforeach
                    <?php if ($check==0): ?>
                        <div class="aside-nav-head hidden">
                            @include ('admin::layouts.nav-aside',['currentKey'=>$menuItem['key']])
                        </div>
                    <?php else: ?>
                        <div class="aside-nav-head">
                            @include ('admin::layouts.nav-aside',['currentKey'=>$menuItem['key']])
                        </div>
                    <?php endif; ?>
                @else
                    <a href="{{ count($menuItem['children']) ? current($menuItem['children'])['url'] : $menuItem['url'] }}">
                        <span class="icon {{ $menuItem['icon-class'] }}"></span>
                        <span>{{ trans($menuItem['name']) }}</span>
                    </a>
                @endif
            </li>
        @endforeach
    </ul>
</div>
