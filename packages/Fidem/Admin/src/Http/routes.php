<?php

Route::group(['middleware' => ['web']], function () {
    Route::prefix('admin')->group(function () {
        // Admin Routes
        Route::group(['middleware' => ['admin']], function () {
            Route::prefix('cms')->group(function () {
                /*
                 * Blog
                 */
                Route::prefix('blog')->group(function () {
                    Route::get('/', 'Fidem\CMS\Http\Controllers\Admin\BlogController@index')->defaults('_config', [
                        'view' => 'fidem_admin::cms.blog.index'
                    ])->name('admin.cms.blog.index');

                    Route::get('create', 'Fidem\CMS\Http\Controllers\Admin\BlogController@create')->defaults('_config', [
                        'view' => 'fidem_admin::cms.blog.create'
                    ])->name('admin.cms.blog.create');

                    Route::post('create', 'Fidem\CMS\Http\Controllers\Admin\BlogController@store')->defaults('_config', [
                        'redirect' => 'admin.cms.blog.index'
                    ])->name('admin.cms.blog.store');

                    Route::get('edit/{id}', 'Fidem\CMS\Http\Controllers\Admin\BlogController@edit')->defaults('_config', [
                        'view' => 'fidem_admin::cms.blog.edit'
                    ])->name('admin.cms.blog.edit');

                    Route::post('edit/{id}', 'Fidem\CMS\Http\Controllers\Admin\BlogController@update')->defaults('_config', [
                        'redirect' => 'admin.cms.blog.index'
                    ])->name('admin.cms.blog.update');

                    Route::post('/delete/{id}', 'Fidem\CMS\Http\Controllers\Admin\BlogController@delete')->defaults('_config', [
                        'redirect' => 'admin.cms.blog.index'
                    ])->name('admin.cms.blog.delete');

                    Route::post('/massdelete', 'Fidem\CMS\Http\Controllers\Admin\BlogController@massDelete')->defaults('_config', [
                        'redirect' => 'admin.cms.blog.index'
                    ])->name('admin.cms.blog.mass-delete');
                });
            });
            /*
             * Translation
             */
            Route::prefix('translations')->group(function () {
                Route::get('/', 'Fidem\Translation\Http\Controllers\Admin\TranslationController@index')->defaults('_config', [
                    'view' => 'fidem_admin::translations.index'
                ])->name('admin.translations.index');

                Route::get('create', 'Fidem\Translation\Http\Controllers\Admin\TranslationController@create')->defaults('_config', [
                    'view' => 'fidem_admin::translations.create'
                ])->name('admin.translation.create');

                Route::post('create', 'Fidem\Translation\Http\Controllers\Admin\TranslationController@store')->defaults('_config', [
                    'redirect' => 'admin.translations.index'
                ])->name('admin.translation.store');

                Route::get('edit/{id}', 'Fidem\Translation\Http\Controllers\Admin\TranslationController@edit')->defaults('_config', [
                    'view' => 'fidem_admin::translations.edit'
                ])->name('admin.translation.edit');

                Route::post('edit/{id}', 'Fidem\Translation\Http\Controllers\Admin\TranslationController@update')->defaults('_config', [
                    'redirect' => 'admin.translations.index'
                ])->name('admin.translation.update');

                Route::post('/delete/{id}', 'Fidem\Translation\Http\Controllers\Admin\TranslationController@delete')->defaults('_config', [
                    'redirect' => 'admin.translations.index'
                ])->name('admin.translation.delete');

                Route::post('/massdelete', 'Fidem\Translation\Http\Controllers\Admin\TranslationController@massDelete')->defaults('_config', [
                    'redirect' => 'admin.translation.index'
                ])->name('admin.translation.mass-delete');
            });

            /*
             * Product import
             */
            Route::post('/catalog/products/import', 'Fidem\Product\Http\Controllers\ProductImportController@import')->defaults('_config', [
                'redirect' => 'admin.catalog.products.index'
            ])->name('admin.catalog.products.import');
        });
    });
});