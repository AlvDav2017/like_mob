<?php

return [
    [
        'key'        => 'cms.blogs',
        'name'       => 'fidem_admin::app.cms.blogs.blogs',
        'route'      => 'admin.cms.blog.index',
        'sort'       => 2,
        'icon-class' => '',
    ],
    [
        'key'        => 'translations',
        'name'       => 'fidem_admin::app.translations.translations',
        'route'      => 'admin.translations.index',
        'sort'       => 13,
        'icon-class' => 'folder-icon',
    ],
    [
        'key'        => 'translations.translations',
        'name'       => 'fidem_admin::app.translations.translations',
        'route'      => 'admin.translations.index',
        'sort'       => 1,
        'icon-class' => '',
    ]
];