<?php

namespace Fidem\Admin\Providers;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

//        $this->loadRoutesFrom(__DIR__.'/../Http/routes.php');
//
//        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'fidem_admin');
//
//        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'fidem_admin');

        $this->publishes([
            __DIR__ . '/../../publishable/assets' => public_path('vendor/fidem/admin/assets'),
        ], 'public');
    }


    /**
     * Register services.
     *
     * @return void
     */
//    public function register()
//    {
//        $this->registerConfig();
//    }

    /**
     * Register package config.
     *
     * @return void
     */
//    protected function registerConfig()
//    {
//        $this->mergeConfigFrom(
//            dirname(__DIR__) . '/Config/menu.php', 'menu.admin'
//        );
//    }
}
