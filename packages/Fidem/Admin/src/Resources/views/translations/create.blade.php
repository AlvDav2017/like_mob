@extends('admin::layouts.content')

@section('page_title')
    {{ __('fidem_admin::app.translations.add-title') }}
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.translation.store') }}" @submit.prevent="onSubmit">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('fidem_admin::app.translations.add-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('fidem_admin::app.translations.create-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">

                <div class="form-container">
                    @csrf()

                    {!! view_render_event('bagisto.admin.translations.create_form_accordian.general.before') !!}

                    <accordian :title="'{{ __('admin::app.cms.pages.general') }}'" :active="true">
                        <div slot="body">
                            @inject('channels', 'Webkul\Core\Repositories\ChannelRepository')

                            <div class="control-group" :class="[errors.has('channels[]') ? 'has-error' : '']">
                                <label for="key" class="required">{{ __('fidem_admin::app.translations.channel') }}</label>

                                <select type="text" class="control" name="channels[]" v-validate="'required'" value="{{ old('channel[]') }}" data-vv-as="&quot;{{ __('fidem_admin::app.translations.channel') }}&quot;" multiple="multiple">
                                    @foreach($channels->all() as $channel)
                                        <option value="{{ $channel->id }}">{{ $channel->name }}</option>
                                    @endforeach
                                </select>

                                <span class="control-error" v-if="errors.has('channels[]')">@{{ errors.first('channels[]') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('key') ? 'has-error' : '']">
                                <label for="key" class="required">{{ __('fidem_admin::app.translations.key') }}</label>

                                <input type="text" class="control" name="key" v-validate="'required'" value="{{ old('key') }}" data-vv-as="&quot;{{ __('fidem_admin::app.translations.key') }}&quot;">

                                <span class="control-error" v-if="errors.has('key')">@{{ errors.first('key') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('value') ? 'has-error' : '']">
                                <label for="value" class="required">{{ __('fidem_admin::app.translations.value') }}</label>

                                <textarea type="text" class="control" id="value" name="value" v-validate="'required'" data-vv-as="&quot;{{ __('fidem_admin::app.translations.value') }}&quot;">{{ old('value') }}</textarea>

                                <span class="control-error" v-if="errors.has('value')">@{{ errors.first('value') }}</span>
                            </div>
                        </div>
                    </accordian>

                    {!! view_render_event('bagisto.admin.translations.create_form_accordian.general.after') !!}
                </div>
            </div>
        </form>
    </div>
@stop