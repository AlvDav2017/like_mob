<?php

namespace Fidem\CartApiToken\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Checkout\Repositories\CartRepository;

class CartApiToken extends Model
{
    public function cart()
    {
        return $this->belongsTo(CartRepository::class);
    }
}
