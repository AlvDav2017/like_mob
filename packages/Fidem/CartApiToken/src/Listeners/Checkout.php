<?php

namespace Fidem\CartApiToken\Listeners;

use Fidem\CartApiToken\Models\CartApiToken;

class Checkout
{

    /**
     * After the cart item add
     *
     * @param  $result
     * @return void
     */
    public function afterCartItemAdded($result)
    {
        $token = request()->input('api_token');

        if( ! empty($token)
            && ! is_null($cart_api_token = CartApiToken::where('api_token', $token)->first())
            && is_null($cart_api_token->cart_id) && session()->has('cart') ) {
            $cart_api_token->cart_id = session()->get('cart')->id;
            $cart_api_token->save();
        }
    }

    /**
     * After the order is created
     *
     * @param  \Webkul\Sales\Contracts\Order  $order
     * @return void
     */
    public function afterCreatedOrder($order)
    {
        if( ! is_null($cart_api_token = CartApiToken::where('api_token', request()->input('api_token'))->first())
            && ! is_null($cart_api_token->cart_id) ) {
            $cart_api_token->cart_id = null;
            $cart_api_token->save();
        }
    }
}