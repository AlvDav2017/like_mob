<?php

Route::group(['prefix' => 'api'], function ($router) {

    Route::group(['middleware' => ['locale', 'theme', 'currency']], function ($router) {
        //Checkout routes
        Route::group(['prefix' => 'checkout'], function ($router) {
            Route::get('cart/token', 'Fidem\CartApiToken\Http\Controllers\CartApiTokenController@get');

            Route::group(['namespace' => 'Webkul\API\Http\Controllers\Shop', 'middleware' => 'cart-api-token'], function ($router) {

                Route::post('cart/add/{id}', 'CartController@store');

                Route::get('cart', 'CartController@get');

                Route::get('cart/empty', 'CartController@destroy');

                Route::put('cart/update', 'CartController@update');

                Route::get('cart/remove-item/{id}', 'CartController@destroyItem');

                Route::get('cart/move-to-wishlist/{id}', 'CartController@moveToWishlist');

                Route::post('save-address', 'CheckoutController@saveAddress');

                Route::post('save-shipping', 'CheckoutController@saveShipping');

                Route::post('save-payment', 'CheckoutController@savePayment');

                Route::post('save-order', 'CheckoutController@saveOrder');
            });
        });
    });
});