<?php

namespace Fidem\CartApiToken\Http\Controllers;

class CartApiTokenController extends Controller
{
    /**
     * Returns a individual resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $token = hash('sha256', \Illuminate\Support\Str::random(60));
        $api_token = new \Fidem\CartApiToken\Models\CartApiToken();
        $api_token->api_token = $token;
        $api_token->save();

        return response()->json([
                'api_token' => $token
            ]
        );
    }
}
