<?php

namespace Fidem\CartApiToken\Http\Middleware;

use Closure;
use Fidem\CartApiToken\Models\CartApiToken as CartApiTokenModel;
use Webkul\Checkout\Repositories\CartRepository;

class CartApiToken
{
    /**
     * CartRepository instance
     *
     * @var \Webkul\Checkout\Repositories\CartRepository
     */
    protected $cartRepository;

    /**
     * @param \Webkul\Checkout\Repositories\CartRepository $cartRepository
     */
    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($cartApiToken = CartApiTokenModel::where('api_token', $request->input('api_token', null))->first()) {
            $customer = auth('api')->user();

            if($customer) {
                $cart = $this->cartRepository->findOneWhere([
                    'customer_id' => $customer->id,
                    'is_active'   => 1,
                ]);

                if($cart) {
                    if( $cartApiToken->cart_id ) {
                        $cartApiToken->cart_id = null;
                        $cartApiToken->save();
                    }
                } elseif ( $cart = $this->cartRepository->find($cartApiToken->cart_id) ) {
                    $cart->is_guest = 0;
                    $cart->customer_id = $customer->id;
                    $cart->save();
                }
            } elseif( $cart = $this->cartRepository->find($cartApiToken->cart_id) ) {
                session()->put('cart', $cart);
            } else {
                session()->forget('cart');
            }
        }

        return $next($request);
    }
}