<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartApiTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_api_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('api_token');
            $table->integer('cart_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('cart_id')->references('id')->on('cart')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_api_tokens');
    }
}
