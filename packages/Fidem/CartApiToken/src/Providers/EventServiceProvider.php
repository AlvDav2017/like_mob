<?php

namespace Fidem\CartApiToken\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen('checkout.cart.item.add.after', 'Fidem\CartApiToken\Listeners\Checkout@afterCartItemAdded');
        Event::listen('checkout.order.save.after', 'Fidem\CartApiToken\Listeners\Checkout@afterCreatedOrder');
    }
}