<?php

namespace Fidem\CartApiToken\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Fidem\CartApiToken\Http\Middleware\CartApiToken as CartApiTokenMiddleware;

class CartApiTokenServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $router->aliasMiddleware('cart-api-token', CartApiTokenMiddleware::class);

        $this->loadRoutesFrom(__DIR__ . '/../Http/routes.php');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->app->register(EventServiceProvider::class);
    }
}
