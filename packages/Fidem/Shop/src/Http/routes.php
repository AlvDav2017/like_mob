<?php

Route::group(['middleware' => ['web', 'locale', 'theme', 'currency']], function () {
    Route::get('blog/{slug}', 'Fidem\CMS\Http\Controllers\Shop\BlogPresenterController@presenter')->name('shop.cms.blog');
});
