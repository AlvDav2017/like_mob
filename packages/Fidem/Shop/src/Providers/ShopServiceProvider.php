<?php

namespace Fidem\Shop\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class ShopServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../Http/routes.php');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'fidem_shop');
    }
}