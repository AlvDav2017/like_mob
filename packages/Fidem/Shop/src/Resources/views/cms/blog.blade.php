@extends('shop::layouts.master')

@section('page_title')
    {{ $blog->blog_title }}
@endsection

@section('seo')
    <meta name="title" content="{{ $blog->meta_title }}" />

    <meta name="description" content="{{ $blog->meta_description }}" />

    <meta name="keywords" content="{{ $blog->meta_keywords }}" />
@endsection

@section('content-wrapper')
    {!! DbView::make($blog)->field('html_content')->render() !!}
@endsection