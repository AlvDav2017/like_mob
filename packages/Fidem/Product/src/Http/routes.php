<?php

Route::group(['prefix' => 'api'], function ($router) {

    Route::group(['namespace' => 'Fidem\Product\Http\Controllers\Shop', 'middleware' => ['locale', 'theme', 'currency']], function ($router) {
        //Product routes
        Route::get('products', 'ProductController@index');
    });
});