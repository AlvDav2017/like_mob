<?php

namespace Fidem\Product\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Webkul\API\Http\Resources\Catalog\Product as ProductResource;
use Webkul\API\Http\Resources\Catalog\Attribute as AttributeResource;
use Webkul\Attribute\Repositories\AttributeRepository;
use Webkul\Product\Models\ProductAttributeValue;
use Webkul\Product\Repositories\ProductFlatRepository;

class ProductCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'Webkul\API\Http\Resources\Catalog\Product';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $productIds = $this->collection->pluck('product_id')->toArray();

        return [
            'data' => ProductResource::collection($this->collection),
            'filter' => AttributeResource::collection($this->getProductFilterAttributes($productIds)),
            'max_price' => app(ProductFlatRepository::class)->whereIn('product_id', $productIds)->max('price')
        ];
    }

    /**
     * @return array
     */
    public function getProductFilterAttributes($productIds)
    {
        $columns = ['attributes.*'];


         foreach (array_unique(array_values(ProductAttributeValue::$attributeTypeFields)) as $column) {
            $columns[] = 'product_attribute_values.' . $column;
        }

        $query = app(AttributeRepository::class)->distinct()
            ->addSelect($columns)
            ->leftJoin('product_attribute_values', 'attributes.id', '=', 'product_attribute_values.attribute_id')
            ->whereIn('product_attribute_values.product_id', $productIds)
            ->where('attributes.is_filterable', 1);

        $attributes = $query->get();

        $query = app(AttributeRepository::class)->distinct()
            ->addSelect('attributes.*')
            ->leftJoin('attribute_options', 'attributes.id', '=', 'attribute_options.attribute_id')
            ->where(function ($q) use ($attributes) {
                $options = [];

                foreach ($attributes as $attribute) {
                    $column = ProductAttributeValue::$attributeTypeFields[$attribute->type];
                    $values = explode(",", $attribute->{$column});

                    foreach ($values as $value) {

                        if (! is_numeric($value)) {
                            continue;
                        }

                        $options[] = $value;
                    }
                }

                $q->whereIn('attribute_options.id', $options);
            })
            ->with('options');


        $results = $query->get();

        return $results;
    }
}