<?php

namespace Fidem\Product\Http\Controllers;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;
use Webkul\Category\Models\CategoryTranslation;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\Product\Repositories\ProductImageRepository;
use Webkul\Product\Models\ProductAttributeValue;
use Webkul\Product\Repositories\ProductAttributeValueRepository;
use Webkul\Attribute\Repositories\AttributeRepository;
use Webkul\Admin\Imports\DataGridImport;
use Illuminate\Support\Facades\Validator;
use Excel;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Support\Facades\Log;

class ProductImportController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ProductRepository object
     *
     * @var \Webkul\Product\Repositories\ProductRepository
     */
    protected $productRepository;

    /**
     * ProductImageRepository object
     *
     * @var \Webkul\Product\Repositories\ProductImageRepository
     */
    protected $productImageRepository;

    /**
     * ProductImageRepository object
     *
     * @var \Webkul\Product\Repositories\ProductAttributeValueRepository
     */
    protected $productAttributeValueRepository;

    /**
     * ProductRepository object
     *
     * @var \Webkul\Attribute\Repositories\AttributeRepository
     */
    protected $attributeRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Product\Repositories\ProductRepository  $productRepository
     * @param  \Webkul\Product\Repositories\ProductImageRepository  $productImageRepository
     * @param  \Webkul\Product\Repositories\ProductAttributeValueRepository  $productAttributeValueRepository
     * @param  \Webkul\Attribute\Repositories\AttributeRepository  $attributeRepository
     * @return void
     */
    public function __construct(
        ProductRepository $productRepository,
        ProductImageRepository $productImageRepository,
        ProductAttributeValueRepository $productAttributeValueRepository,
        AttributeRepository $attributeRepository)
    {
        $this->productRepository = $productRepository;
        $this->productImageRepository = $productImageRepository;
        $this->productAttributeValueRepository = $productAttributeValueRepository;
        $this->attributeRepository = $attributeRepository;

        $this->_config = request('_config');
    }

    /**
     * import function for the upload
     *
     * @return \Illuminate\Http\Response
     */
    public function import()
    {
        $valid_extension = ['xlsx', 'csv', 'xls'];

        if (! in_array(request()->file('file')->getClientOriginalExtension(), $valid_extension)) {
            session()->flash('error', trans('admin::app.export.upload-error'));
        } else {
            try {
                $start = microtime(true);
                ini_set('max_execution_time', -1);
                ini_set('memory_limit', "1024M");
                $excelData = (new DataGridImport)->toArray(request()->file('file'));

                foreach ($excelData as $data) {
                    foreach ($data as $column => $uploadData) {
                        $sku[$column+1] = $uploadData['sku'];
                    }

                    $skuCount = array_count_values($sku);

                    $filtered = array_filter($sku, function ($value) use ($skuCount) {
                        return $skuCount[$value] > 1;
                    });
                }

                if ($filtered) {
                    foreach ($filtered as $position => $sku) {
                        $message[] = trans('admin::app.export.duplicate-error', ['sku' => $sku, 'position' => $position]);
                    }

                    $finalMsg = implode(" ", $message);

                    session()->flash('error', $finalMsg);
                } else {
                    $products = $this->productRepository->pluck('sku', 'id');

                    foreach ($products as $id => $product_sku)
                        $prodIdentifier[$id] = $product_sku;

                    foreach ($excelData as $data) {
                        $products = []; $errorMsg = [];
                        foreach ($data as $column => $uploadData) {
                            $default_data = [
                                'type' => 'simple',
                                'attribute_family_id' => '1',
                                'sku' => trim($uploadData['sku'])
                            ];

                            if (! isset($prodIdentifier) ||
                                ( isset($prodIdentifier) && ! ($id = array_search($default_data['sku'], $prodIdentifier)) ) ) {
                                $validator = Validator::make($default_data, [
                                    'type' => 'required',
                                    'attribute_family_id' => 'required',
                                    'sku' => ['required', 'unique:products,sku', new \Webkul\Core\Contracts\Validations\Slug]
                                ]);

                                if ($validator->fails()) {
                                    $errorMsg[$column+1] = $validator->errors()->first();
                                } else {
                                    $id = $this->productRepository->create($default_data)->id;

                                    Log::channel('import-products')->info('Log: #ID '.($column+1).'|'.$id.' Product has been created. Time: '.(microtime(true) - $start));
                                }
                            }

                            if (empty($errorMsg)) {
                                $import_data = [
                                    'weight' => trim($uploadData['weight']),
                                    'sku' => $default_data['sku'],
                                    'url_key' => trim($uploadData['url_key']),
                                    'new' => trim($uploadData['new']),
                                    'visible_individually' => 1,
                                    'status' => trim($uploadData['status']),
                                    'inventories' => [1 => trim($uploadData['qty'])],
                                    'featured' => trim($uploadData['sale']),
                                    'short_description' => trim($uploadData['short_description']),
                                    'description' => trim($uploadData['description']),
                                    'meta_title' => trim($uploadData['meta_title']),
                                    'meta_description' => trim($uploadData['meta_description']),
                                    'meta_keywords' => trim($uploadData['meta_keywords']),
                                    'price' => trim($uploadData['price']),
                                    'special_price' => trim($uploadData['special_price']),
                                    'categories' => [],
                                    'images' => [],
                                ];

                                /*Set Categories*/
                                $multi_categories = explode(",", trim($uploadData['categories']));

                                foreach ($multi_categories as $multi_category) {
                                    $categories = explode("///", trim($multi_category));

                                    foreach ($categories as $cat_name) {

                                        if ($category_translation = CategoryTranslation::where('name', $cat_name)->first())
                                            $import_data['categories'][] = $category_translation->category_id;
                                    }
                                }

                                /*Set Attributes*/
                                $attributes = [
                                    'brands',
                                    'lamp_type',
                                    'insulation_type',
                                    'power',
                                    'voltage',
                                    'color_range',
                                ];

                                foreach ($attributes as $code) {

                                    if ($attribute = $this->attributeRepository->where('code', $code)->first()) {
                                        $code_attributes = explode("///", trim($uploadData[$code]));
                                        $import_data[$code] = [];

                                        foreach ($code_attributes as $code_attribute) {

                                            if ($option = $attribute->options->where('admin_name', $code_attribute)->first())
                                                $import_data[$code][] = $option->id;
                                        }
                                    }
                                }

                                /*Set Images*/
                                $images = [];
                                $additional_images = trim($uploadData['additional_images']);
                                if ($additional_images)
                                    $images = array_merge($images, explode("///", $additional_images));

                                $main_image = trim($uploadData['main_image']);
                                if ($main_image)
                                    $images[] = $main_image;

                                foreach ($images as $path) {

                                    if (Storage::exists('images/'.$path)) {

                                        if(! Storage::exists('product/'.$id.'/'.$path))
                                            Storage::copy('images/'.$path, 'product/'.$id.'/'.$path);

                                        $image = $this->productImageRepository->firstOrCreate([
                                            'path' => 'product/'.$id.'/'.$path,
                                            'product_id' => $id,
                                        ]);
                                        $image->type = null;

                                        if ($main_image && $path === $main_image)
                                            $image->type = 'main';

                                        $image->save();

                                        Log::channel('import-products')->info('Log: #ID '.($column+1).'|'.$id.'|'.$image->id.' Image has been updated. Time: '.(microtime(true) - $start));
                                    }
                                }

                                foreach ($this->productRepository->find($id)->images()->pluck('path', 'id') as $imageId => $path) {
                                    $image_name = str_replace("product/".$id."/", "", $path);

                                    if (array_search($image_name, $images) === false) {
                                        $this->productImageRepository->delete($imageId);

                                        if(Storage::exists($path))
                                            Storage::delete($path);
                                    }

                                    $import_data['images'][$imageId] = '';
                                }

                                /*Set Relations*/
                                $relations = [
                                    'related_products' => explode(",", trim($uploadData['related_products'])),
                                    'cross_sell' => explode(",", trim($uploadData['cross_sell_products']))
                                ];

                                foreach (core()->getAllChannels() as $channel) {
                                    $import_data['channel'] = $channel->code;
                                    $import_data['channels'] = [$channel->id];

                                    foreach (core()->getAllLocales()->reverse() as $locale) {
                                        $import_data['locale'] = $locale->code;
                                        $lang = $import_data['locale'] !== app()->getLocale() ? '_'.$import_data['locale'] : '';
                                        $import_data['name'] = $uploadData['name'.$lang];

                                        $products[$id][$import_data['locale']] = array_merge($import_data, ['relations' => $relations]);
                                    }
                                }
                            }
                        }

                        $column = 0;
                        foreach ($products as $id => $locales) {
                            $column++;
                            foreach ($locales as $import_data) {
                                foreach ($import_data['relations'] as $relation => $indexes) {
                                    foreach ($indexes as $index) {
                                        if (isset($sku[$index]) && ( $product = $this->productRepository->where('sku', $sku[$index])->first() ))
                                            $import_data[$relation][] = $product->id;
                                    }
                                }

                                $validator = Validator::make($import_data, [
                                    'sku' => ['required', 'unique:products,sku,' . $id, new \Webkul\Core\Contracts\Validations\Slug],
                                    'url_key' => ['required', function ($field, $value, $fail) use ($id, $import_data) {
                                        $column = ProductAttributeValue::$attributeTypeFields['text'];

                                        if (!$this->productAttributeValueRepository->isValueUnique($id, 3, $column, $import_data['url_key'])) {
                                            $fail('The :attribute has already been taken.');
                                        }
                                    }],
                                    'name' => 'required',
                                    'description' => 'required',
                                    'price' => ['required', new \Webkul\Core\Contracts\Validations\Decimal],
                                    'special_price' => ['nullable', new \Webkul\Core\Contracts\Validations\Decimal, 'lt:price', 'gt:price']
                                ]);

                                if ($validator->fails()) {
                                    $errorMsg[$column] = $validator->errors()->first();
                                } else {
                                    $this->productRepository->update($import_data, $id);

                                    Log::channel('import-products')->info('Log: #ID '.($column).'|'.$id.'|'.$import_data['locale'].' Product has been updated. Time: '.(microtime(true) - $start));
                                }
                            }
                        }
                    }

                    if(! empty($errorMsg)) {
                        foreach ($errorMsg as $key => $msg) {
                            $msg = str_replace(".", "", $msg);
                            $message[] = $msg. ' at Row '  .$key . '.';
                        }

                        $finalMsg = implode(" ", $message);

                        session()->flash('error', $finalMsg);
                    } else {
                        session()->flash('success', trans('admin::app.response.upload-success', ['name' => 'Product']));
                    }
                }
            } catch (\Exception $e) {
                session()->flash('error', $e->getMessage().' '.$e->getFile().' '.$e->getLine());
            }
        }

        return redirect()->route($this->_config['redirect']);
    }
}