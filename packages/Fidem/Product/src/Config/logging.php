<?php

return [
    'import-products' => [
        'driver' => 'single',
        'path' => storage_path('logs/import-products.log'),
        'level' => 'debug',
    ]
];
