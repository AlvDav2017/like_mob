<?php

namespace Fidem\Product\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class ProductServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
     
        $this->loadRoutesFrom(__DIR__ . '/../Http/routes.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerFacades();
    }

    /**
     * Register Bouncer as a singleton.
     *
     * @return void
     */
    protected function registerFacades()
    {
        $loader = AliasLoader::getInstance();

        $loader->alias(\Webkul\Product\Type\AbstractType::class, \Fidem\Product\Type\AbstractType::class);
        $loader->alias(\Webkul\Product\Type\Simple::class, \Fidem\Product\Type\Simple::class);
        $loader->alias(\Webkul\Product\Repositories\ProductRepository::class, \Fidem\Product\Repositories\ProductRepository::class);
    }
}
