<?php

namespace Fidem\Product\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class ProductImportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/logging.php', 'logging.channels'
        );
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }
}
