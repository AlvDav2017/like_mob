<?php

namespace Fidem\Attribute\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class AttributeDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    protected $attributeId;

    /**
     * Create a new resource instance.
     *
     * @return void
     */
    public function __construct($attributeId = '')
    {
        $this->attributeId = $attributeId;

        parent::__construct();
    }

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('attribute_options')
            ->select('attribute_options.id', 'attribute_options.admin_name')
            ->leftJoin('attributes', 'attribute_options.attribute_id', '=', 'attributes.id')
            ->where('attributes.id', $this->attributeId);

        $this->addFilter('id', 'attribute_options.id');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'id',
            'label'      => trans('admin::app.datagrid.id'),
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'admin_name',
            'label'      => trans('admin::app.catalog.attributes.admin_name'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
    }

    public function prepareActions()
    {
		$this->addAction([
			'title'  => trans('admin::app.datagrid.edit'),
			'method' => 'GET',
			'route'  => 'admin.catalog.brands.edit',
			'icon'   => 'icon pencil-lg-icon',
		]);
    }
}