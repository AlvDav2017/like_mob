<?php

return [
    'brands' => [
        'title' => 'Brands',
        'add-title' => 'Add Brand',
        'create-btn-title' => 'Save Brand',
        'edit-title' => 'Edit Brand',
        'edit-btn-title' => 'Save Brand',
        'translation-title' => 'Brand Title'
    ]
];
