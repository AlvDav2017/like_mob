@extends('admin::layouts.content')

@section('page_title')
    {{ __('attribute::app.brands.title') }}
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('attribute::app.brands.title') }}</h1>
            </div>
            <div class="page-action">
                <a href="{{ route('admin.catalog.attributes.edit', $attribute->id) }}" class="btn btn-lg btn-primary">
                    {{ __('attribute::app.brands.add-title') }}
                </a>
            </div>
        </div>

        <div class="page-content">
            {!! $dataGrid->render() !!}
        </div>
    </div>
@stop

