@extends('admin::layouts.content')

@section('page_title')
    {{ __('attribute::app.brands.edit-title') }}
@stop

@section('content')
    <div class="content">
        <form method="POST" id="page-form" action="" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('attribute::app.brands.edit-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('attribute::app.brands.edit-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">

                <div class="form-container">
                    @csrf()

                    <div class="control-group {!! $errors->has('image.*') ? 'has-error' : '' !!}">
                        <label>{{ __('admin::app.catalog.categories.image') }}</label>

                        <image-wrapper :button-label="'{{ __('admin::app.catalog.products.add-image-btn-title') }}'" input-name="path" :multiple="false"  :images='"{{ $attributeOption->image_url }}"'></image-wrapper>

                        <span class="control-error" v-if="{!! $errors->has('path.*') !!}">
                            @foreach ($errors->get('path.*') as $key => $message)
                                @php echo str_replace($key, 'Image', $message[0]); @endphp
                            @endforeach
                        </span>

                    </div>
                </div>
            </div>
        </form>
    </div>
@stop