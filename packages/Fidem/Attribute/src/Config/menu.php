<?php

return [
    [
        'key'        => 'catalog.brands',
        'name'       => 'attribute::app.brands.title',
        'route'      => 'admin.catalog.brands.index',
        'sort'       => 5,
        'icon-class' => '',
    ]
];