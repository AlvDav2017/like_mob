<?php

namespace Fidem\Attribute\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Fidem\Attribute\Contracts\AttributeOptionImage as AttributeOptionImageContract;
use Webkul\Attribute\Models\AttributeOptionProxy;

class AttributeOptionImage extends Model implements AttributeOptionImageContract
{
    public $timestamps = false;

    protected $fillable = [
        'path',
        'attribute_option_id',
    ];

    /**
     * Get the product that owns the image.
     */
    public function attributeOption()
    {
        return $this->belongsTo(AttributeOptionProxy::modelClass());
    }
}