<?php

Route::group(['middleware' => ['web']], function () {
    // Admin Routes
    Route::prefix('admin')->group(function () {
        Route::group(['namespace' => 'Fidem\Attribute\Http\Controllers\Admin', 'middleware' => ['admin']], function () {
            //Translation routes
            Route::prefix('catalog')->group(function () {
                Route::prefix('brands')->group(function () {
                    Route::get('/', 'AttributeController@index')->defaults('_config', [
                        'view' => 'attribute::attributes.index',
                        'attribute' => 'brand'
                    ])->name('admin.catalog.brands.index');

                    Route::get('edit/{id}', 'AttributeController@edit')->defaults('_config', [
                        'view' => 'attribute::attributes.edit',
                    ])->name('admin.catalog.brands.edit');

                    Route::post('edit/{id}', 'AttributeController@update')->defaults('_config', [
                        'redirect' => 'admin.catalog.brands.index',
                        'attribute' => 'brand'
                    ])->name('admin.catalog.brands.update');
                });
            });
        });
    });
});