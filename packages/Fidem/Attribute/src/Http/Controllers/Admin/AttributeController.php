<?php

namespace Fidem\Attribute\Http\Controllers\Admin;

use Fidem\Attribute\Http\Controllers\Controller;
use Fidem\Attribute\Repositories\AttributeOptionImageRepository;
use Webkul\Attribute\Repositories\AttributeRepository;
use Webkul\Attribute\Repositories\AttributeOptionRepository;
use Fidem\Attribute\DataGrids\AttributeDataGrid;

 class AttributeController extends Controller
{
    /**
     * To hold the request variables from route file
     * 
     * @var array
     */
    protected $_config;

    /**
     * To hold the CMSRepository instance
     * 
     * @var \Fidem\Attribute\Repositories\AttributeOptionImageRepository
     */
    protected $attributeOptionImageRepository;

    /**
     * To hold the AttributeRepository instance
     *
     * @var \Webkul\Attribute\Repositories\AttributeRepository
     */
    protected $attributeRepository;

    /**
     * To hold the AttributeOptionRepository instance
     *
     * @var \Webkul\Attribute\Repositories\AttributeOptionRepository
     */
    protected $attributeOptionRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Fidem\Attribute\Repositories\AttributeOptionImageRepository  $attributeOptionImageRepository
     * @param  \Webkul\Attribute\Repositories\AttributeRepository  $attributeRepository
     * @param  \Webkul\Attribute\Repositories\AttributeOptionRepository  $attributeOptionRepository
     * @return void
     */
    public function __construct(
        AttributeOptionImageRepository $attributeOptionImageRepository,
        AttributeRepository $attributeRepository,
        AttributeOptionRepository $attributeOptionRepository)
    {
        $this->middleware('admin');

        $this->attributeOptionImageRepository = $attributeOptionImageRepository;

        $this->attributeRepository = $attributeRepository;

        $this->attributeOptionRepository = $attributeOptionRepository;

        $this->_config = request('_config');
    }

    /**
     * Loads the index blog showing the static blogs resources
     * 
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $attribute = $this->attributeRepository->where('code', $this->_config['attribute'])->firstOrFail();

        $dataGrid = new AttributeDataGrid($attribute->id);
        
        return view($this->_config['view'], compact('dataGrid', 'attribute'));
    }

    /**
     * To edit a previously created translation
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $attributeOption = $this->attributeOptionRepository->findOrFail($id);

        return view($this->_config['view'], compact('attributeOption'));
    }

    /**
     * To update the previously created brand in storage
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'path.*'         => 'mimes:jpeg,jpg,bmp,png',
        ]);

        $attributeOption = $this->attributeOptionRepository->findOrFail($id);

        $attributeOptionImage = $this->attributeOptionImageRepository->firstOrCreate(['attribute_option_id' => $id]);

        $this->attributeOptionImageRepository->uploadImages(request()->all(), $attributeOption);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Attribute Option']));

        return redirect()->route($this->_config['redirect']);
    }
}