<?php

namespace Fidem\Attribute\Http\Resources\Catalog;

use Illuminate\Http\Resources\Json\JsonResource;

class AttributeOption extends JsonResource
{
    /**
     * Create a new resource instance.
     *
     * @return void
     */
    public function __construct($resource)
    {
        $this->attributeOptionImageHelper = app('Fidem\Attribute\Helpers\AttributeOptionImage');

        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'admin_name'   => $this->admin_name,
            'label'        => $this->label,
            'swatch_value' => $this->swatch_value,
            'image'        => $this->attributeOptionImageHelper->getAttributeOptionBaseImage($this)
        ];
    }
}