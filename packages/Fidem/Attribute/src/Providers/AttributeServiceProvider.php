<?php

namespace Fidem\Attribute\Providers;

use Illuminate\Support\ServiceProvider;
use Fidem\Attribute\Providers\ModuleServiceProvider;
use Illuminate\Foundation\AliasLoader;

class AttributeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../Http/routes.php');

        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'attribute');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'attribute');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfig();
    }

    /**
     * Register package config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/menu.php', 'menu.admin'
        );

        $loader = AliasLoader::getInstance();
        $loader->alias(\Webkul\Attribute\Models\AttributeOption::class, \Fidem\Attribute\Models\AttributeOption::class);
        $loader->alias(\Webkul\API\Http\Resources\Catalog\AttributeOption::class, \Fidem\Attribute\Http\Resources\Catalog\AttributeOption::class);
    }
}