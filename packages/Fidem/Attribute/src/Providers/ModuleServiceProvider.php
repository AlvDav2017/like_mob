<?php

namespace Fidem\Attribute\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Fidem\Attribute\Models\AttributeOptionImage::class
    ];
}