<?php

namespace Fidem\Attribute\Repositories;

use Illuminate\Support\Facades\Storage;
use Webkul\Core\Eloquent\Repository;
use Illuminate\Support\Str;

class AttributeOptionImageRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Fidem\Attribute\Contracts\AttributeOptionImage';
    }

    /**
     * @param  array  $data
     * @param  \Webkul\Attribute\Contracts\AttributeOption  $attributeOption
     * @return void
     */
    public function uploadImages($data, $attributeOption, $type = "path")
    {

      //  Log::channel('test')->info('test:  images');
        if (isset($data[$type])) {
            $request = request();

            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'brand/' . $attributeOption->id;

                if ($request->hasFile($file)) {
                    if ($attributeOption->image->{$type}) {
                        Storage::delete($attributeOption->image->{$type});
                    }

                    $attributeOption->image->{$type} = $request->file($file)->store($dir);

                   // Log::channel('test')->info('Log:  '.$request->file($file)->store($dir).' dir'.$dir);
                    $attributeOption->image->save();
                }
            }
        } else {
            if ($attributeOption->image->{$type}) {
                Storage::delete($attributeOption->image->{$type});
            }

            $attributeOption->image->{$type} = null;
            $attributeOption->save();
        }
    }
}