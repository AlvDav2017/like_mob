<?php

namespace Fidem\Attribute\Helpers;

use Illuminate\Support\Facades\Storage;

class AttributeOptionImage
{
    /**
     * Get product's base image
     *
     * @param  \Webkul\Attribute\Contracts\AttributeOption $attributeOption
     * @return array
     */
    public function getAttributeOptionBaseImage($attributeOption)
    {


        $image = $attributeOption ? $attributeOption->image  : null;


        if (! $image) {
            $image = asset('vendor/webkul/ui/assets/images/product/large-product-placeholder.png');
        }else{

            $image='https://prod.haysell.com/images/upload/category/thumb/450/480/'.$image->path;
        }


         return  $image ;
    }
}