<?php

Route::group(['middleware' => ['web']], function () {
    Route::prefix('evoca/vpos')->group(function () {

        Route::get('/redirect', 'Fidem\AmeriaVpos\Http\Controllers\VposController@redirect')->name('ameria.vpos.redirect');

        Route::get('/success', 'Fidem\AmeriaVpos\Http\Controllers\VposController@success')->name('ameria.vpos.success');

        Route::get('/cancel', 'Fidem\AmeriaVpos\Http\Controllers\VposController@cancel')->name('ameria.vpos.cancel');
    });
});

Route::get('evoca/vpos/ipn', 'Fidem\AmeriaVpos\Http\Controllers\VposController@ipn')->name('ameria.vpos.ipn');

Route::post('evoca/vpos/ipn', 'Fidem\AmeriaVpos\Http\Controllers\VposController@ipn')->name('ameria.vpos.ipn');
