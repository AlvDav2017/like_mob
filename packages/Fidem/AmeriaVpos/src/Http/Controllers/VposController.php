<?php

namespace Fidem\AmeriaVpos\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Webkul\Checkout\Facades\Cart;
use Webkul\Sales\Repositories\OrderRepository;
use Fidem\AmeriaVpos\Helpers\Ipn;

class VposController extends Controller
{
    /**
     * OrderRepository object
     *
     * @var \Webkul\Sales\Repositories\OrderRepository
     */
    protected $orderRepository;

    /**
     * Ipn object
     *
     * @var \Fidem\AmeriaVpos\Helpers\Ipn
     */
    protected $ipnHelper;

    protected $ameriaVpos;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Attribute\Repositories\OrderRepository  $orderRepository
     * @return void
     */
    public function __construct(
        OrderRepository $orderRepository,
        Ipn $ipnHelper
    )
    {
        $this->orderRepository = $orderRepository;

        $this->ipnHelper = $ipnHelper;

        $this->ameriaVpos = app('Fidem\AmeriaVpos\Payment\Vpos');
    }

    /**
     * Redirects to the paypal.
     *
     * @return \Illuminate\View\View
     */
    public function redirect()
    {


        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->ameriaVpos->getAmeriaVposInitPaymentUrl());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->ameriaVpos->getFormFields()));
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode($this->ameriaVpos->getFormFields())),
            'Accept: application/json'
        ));
        $webService = json_decode(curl_exec($curl));
        curl_close($curl);


        if ($webService->errorCode == '0' && $webService->orderId !== '') {
            $redirecturl = $webService->formUrl;
        } else {
            $error_message = $webService->errorMessage;

            return view('ameria::vpos-redirect', compact('error_message'));
        }

        return view('ameria::vpos-redirect', compact('redirecturl'));
    }

    /**
     * Cancel payment from paypal.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
        session()->flash('error', 'Evoca payment has been canceled.');

        return redirect()->route('shop.checkout.cart.index');
    }

    /**
     * Success payment
     *
     * @return \Illuminate\Http\Response
     */
    public function success()
    {



        if( request()->has('orderId')) {
             $params = [
                'orderId' =>   request()->input('orderId'),
                'userName'  => $this->ameriaVpos->getConfigData('username'),
                'password'  => $this->ameriaVpos->getConfigData('password')
            ];

            $client = new \GuzzleHttp\Client();
            $res=  $client->post(
                $this->ameriaVpos->getAmeriaVposPaymentDetailsUrl(),
                array(
                    'form_params' => $params
                )
            );
             $webService = json_decode($res->getBody());

              if ($webService->errorCode == '0' && $webService->orderStatus == '6') {
                session()->put('cart', \Webkul\Checkout\Models\Cart::find($webService->orderNumber));

                $order = $this->orderRepository->create(Cart::prepareDataForOrder());

                Cart::deActivateCart();

                session()->flash('order', $order);
                $params = '';
//                if( $this->ameriaVpos->getConfigData('sandbox') ) {
//                    $params .= '&sandbox=1';
//                }

                return redirect('http://fooddepot.fidem.am/thanks?order='.$order->id.'&id='.$params);
            }
        }

    }

    /**
     * Paypal Ipn listener
     *
     * @return \Illuminate\Http\Response
     */
    public function ipn()
    {
        $this->ipnHelper->processIpn(request()->all());
    }
}