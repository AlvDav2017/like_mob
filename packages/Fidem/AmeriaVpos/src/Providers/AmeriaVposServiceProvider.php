<?php

namespace Fidem\AmeriaVpos\Providers;

use Illuminate\Support\ServiceProvider;

class AmeriaVposServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
         $this->loadRoutesFrom(__DIR__.'/../Http/routes.php');

        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'admin_ameria');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'ameria');
    }
}
