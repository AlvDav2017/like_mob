<?php

namespace Fidem\AmeriaVpos\Payment;

use Illuminate\Support\Facades\Config;
use Webkul\Payment\Payment\Payment;

abstract class AmeriaVpos extends Payment
{
    /**
     * PayPal web URL generic getter
     *
     * @param  array  $params
     * @return string
     */
    public function getAmeriaVposInitPaymentUrl($params = [])
    {
        return sprintf('https://ipaytest.arca.am:8445/payment/rest/register.do%s',
            $this->getConfigData('sandbox') ? 'test' : '',
            $params ? '?' . http_build_query($params) : ''
        );

//        return sprintf('https://services%s.ameriabank.am/VPOS/api/VPOS/InitPayment%s',
//            $this->getConfigData('sandbox') ? 'test' : '',
//            $params ? '?' . http_build_query($params) : ''
//        );
    }
    /**
     * PayPal web URL generic getter
     *
     * @param  array  $params
     * @return string
     */
    public function getAmeriaVposPayUrl($params = [])
    {
        return sprintf('https://services%s.ameriabank.am/VPOS/Payments/Pay%s',
            $this->getConfigData('sandbox') ? 'test' : '',
            $params ? '?' . http_build_query($params) : ''
        );
    }
    /**
     * PayPal web URL generic getter
     *
     * @param  array  $params
     * @return string
     */
    public function getAmeriaVposPaymentDetailsUrl($params = [])
    {
        return "https://ipaytest.arca.am:8445/payment/rest/getOrderStatusExtended.do";


//        return sprintf('https://ipay.arca.am:8445/payment/rest/getOrderStatusExtended.do%s',
//            $this->getConfigData('sandbox') ? 'test' : '',
//            $params ? '?' . http_build_query($params) : ''
//        );
//        return sprintf('https://ipay.arca.am/payment/rest/getOrderStatusExtended.do%s',
//            $this->getConfigData('sandbox') ? 'test' : '',
//            $params ? '?' . http_build_query($params) : ''
//        );
    }

    /**
     * Add order item fields
     *
     * @param  array  $fields
     * @param  int  $i
     * @return void
     */
    protected function addLineItemsFields(&$fields, $i = 1)
    {
        $cartItems = $this->getCartItems();

        foreach ($cartItems as $item) {
            foreach ($this->itemFieldsFormat as $modelField => $paypalField) {
                $fields[sprintf($paypalField, $i)] = $item->{$modelField};
            }

            $i++;
        }
    }

    /**
     * Add billing address fields
     *
     * @param  array  $fields
     * @return void
     */
    protected function addAddressFields(&$fields)
    {
        $cart = $this->getCart();

        $billingAddress = $cart->billing_address;

        $fields = array_merge($fields, [
            'city'             => $billingAddress->city,
            'country'          => $billingAddress->country,
            'email'            => $billingAddress->email,
            'first_name'       => $billingAddress->first_name,
            'last_name'        => $billingAddress->last_name,
            'zip'              => $billingAddress->postcode,
            'state'            => $billingAddress->state,
            'address1'         => $billingAddress->address1,
            'address_override' => 1,
        ]);
    }

    /**
     * Checks if line items enabled or not
     *
     * @return bool
     */
    public function getIsLineItemsEnabled()
    {
        return true;
    }
}