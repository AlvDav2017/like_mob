<?php

namespace Fidem\AmeriaVpos\Payment;

class Vpos extends AmeriaVpos
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code  = 'ameria_vpos';

    /**
     * Line items fields mapping
     *
     * @var array
     */
    protected $itemFieldsFormat = [
        'id'       => 'item_number_%d',
        'name'     => 'item_name_%d',
        'quantity' => 'quantity_%d',
        'price'    => 'amount_%d',
    ];

    /**
     * Return paypal redirect url
     *
     * @return string
     */
    public function getRedirectUrl()
    {

//        $curl = curl_init();
//
//        curl_setopt($curl, CURLOPT_URL, $this->getAmeriaVposInitPaymentUrl());
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
//        curl_setopt($curl, CURLOPT_POST, true);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->getFormFields()));
//        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
//            'Content-Type: application/json',
//            'Content-Length: ' . strlen(json_encode($this->getFormFields())),
//            'Accept: application/json'
//        ));
//
//
//        $webService = json_decode(curl_exec($curl));

//          curl_close($curl);

        $client = new \GuzzleHttp\Client();
        $res=  $client->post(
            $this->getAmeriaVposInitPaymentUrl(),
            array(
                'form_params' => $this->getFormFields()
            )
        );
        $webService = json_decode($res->getBody());


        if ($webService->errorCode == '0') {

            return $webService->formUrl;

               // route('ameria.vpos.redirect');

//            return $this->getAmeriaVposPayUrl(['id' => $webService->PaymentID, 'lang' => 'am']);
        }

//        return route('ameria.vpos.redirect');
    }

    /**
     * Return form field array
     *
     * @return array
     */
    public function getFormFields()
    {
        $cart = $this->getCart();


         $fields = [
            'userName'        => $this->getConfigData('username'),
            'password'        => $this->getConfigData('password'),
            'description'     => request()->has('description') ? request()->input('description') : core()->getCurrentChannel()->name,
            'orderNumber'     => $this->getConfigData('sandbox') ? rand(2351901, 2352000) : $cart->id,
            'amount'          => $this->getConfigData('sandbox') ? 10 : floatval($cart->sub_total),
            'returnUrl'         => route('ameria.vpos.success'),

//            'invoice'         => $cart->id,
//            'currency_code'   => $cart->cart_currency_code,
//            'paymentaction'   => 'sale',
//            'return'          => route('ameria.vpos.success'),
//            'cancel_return'   => route('ameria.vpos.cancel'),
//            'notify_url'      => route('ameria.vpos.ipn'),
//            'charset'         => 'utf-8',
//            'item_name'       => core()->getCurrentChannel()->name,
//            'amount'          => $cart->sub_total,
//            'tax'             => $cart->tax_total,
//            'shipping'        => $cart->selected_shipping_rate ? $cart->selected_shipping_rate->price : 0,
//            'discount_amount' => $cart->discount_amount,
        ];

//        if ($this->getIsLineItemsEnabled()) {
//            $fields = array_merge($fields, array(
//                'cmd'    => '_cart',
//                'upload' => 1,
//            ));
//
//            $this->addLineItemsFields($fields);
//
//            if ($cart->selected_shipping_rate)
//                $this->addShippingAsLineItems($fields, $cart->items()->count() + 1);
//
//            if (isset($fields['tax'])) {
//                $fields['tax_cart'] = $fields['tax'];
//            }
//
//            if (isset($fields['discount_amount'])) {
//                $fields['discount_amount_cart'] = $fields['discount_amount'];
//            }
//        } else {
//            $fields = array_merge($fields, array(
//                'cmd'           => '_ext-enter',
//                'redirect_cmd'  => '_xclick',
//            ));
//        }
//
//        $this->addAddressFields($fields);

        return $fields;
    }

    /**
     * Add shipping as item
     *
     * @param  array  $fields
     * @param  int    $i
     * @return void
     */
    protected function addShippingAsLineItems(&$fields, $i)
    {
        $cart = $this->getCart();

        $fields[sprintf('item_number_%d', $i)] = $cart->selected_shipping_rate->carrier_title;
        $fields[sprintf('item_name_%d', $i)] = 'Shipping';
        $fields[sprintf('quantity_%d', $i)] = 1;
        $fields[sprintf('amount_%d', $i)] = $cart->selected_shipping_rate->price;
    }
}