<body data-gr-c-s-loaded="true" cz-shortcut-listen="true">
@if(isset($error_message))
    {{ $error_message }}
@else
    <script type="text/javascript">
        window.location.href = '{{ $redirecturl }}';
    </script>
@endif
</body>