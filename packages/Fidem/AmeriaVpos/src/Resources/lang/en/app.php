<?php

return [
    'admin' => [
        'system' => [
            'ameria-vpos'   => 'Evoca vPOS',
            'client_id'     => 'Evoca Client Id',
            'username'      => 'Evoca Account Username',
            'password'      => 'Evoca Account Password',
            'sandbox'       => 'Evoca Test Mode'
       ]
    ]
];
