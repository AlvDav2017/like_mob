<?php

namespace Fidem\CMS\Observers;

use Illuminate\Support\Facades\Storage;

class CmsBlogObserver
{
    /**
     * Handle the Product "deleted" event.
     *
     * @param  \Fidem\CMS\Contracts\CmsBlog  $blog
     * @return void
     */
    public function deleted($blog)
    {
        Storage::deleteDirectory('blogs/' . $blog->id);
    }
}