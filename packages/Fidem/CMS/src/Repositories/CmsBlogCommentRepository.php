<?php

namespace Fidem\CMS\Repositories;

use Illuminate\Container\Container as App;
use Webkul\Core\Eloquent\Repository;

class CmsBlogCommentRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Fidem\CMS\Contracts\CmsBlogComment';
    }

    /**
     * Retrieve comment for customerId
     *
     * @param int $customerId
     */
    function getCustomerComment()
    {
        $customerId = auth()->guard('customer')->user()->id;

        $comments = $this->model->where(['customer_id'=> $customerId])->with('blog')->paginate(5);

        return $comments;
    }
}