<?php

namespace Fidem\CMS\Repositories;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Webkul\Core\Eloquent\Repository;
use Fidem\CMS\Models\CmsBlogTranslation;

class CmsBlogRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Fidem\CMS\Contracts\CmsBlog';
    }

    /**
     * @param  array  $data
     * @return \Fidem\CMS\Contracts\CmsBlog
     */
    public function create(array $data)
    {
        Event::dispatch('cms.blogs.create.before');

        $model = $this->getModel();

        foreach (core()->getAllLocales() as $locale) {
            foreach ($model->translatedAttributes as $attribute) {

                if (isset($data[$attribute])) {
                    $data[$locale->code][$attribute] = $data[$attribute];
                }
            }
        }

        $blog = parent::create($data);

        $this->uploadImages($data, $blog);

        $blog->channels()->sync($data['channels']);

        $blog->related_blogs()->sync($data['related_blogs'] ?? []);

        Event::dispatch('cms.blogs.create.after', $blog);

        return $blog;
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \Fidem\CMS\Contracts\CmsBlog
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $blog = $this->find($id);

        Event::dispatch('cms.blogs.update.before', $id);

        parent::update($data, $id, $attribute);

        $this->uploadImages($data, $blog);

        $blog->channels()->sync($data['channels']);

        $blog->related_blogs()->sync($data['related_blogs'] ?? []);

        Event::dispatch('cms.blogs.update.after', $id);

        return $blog;
    }

    /**
     * Checks slug is unique or not based on locale
     *
     * @param  int  $id
     * @param  string  $urlKey
     * @return bool
     */
    public function isUrlKeyUnique($id, $urlKey)
    {
        $exists = CmsBlogTranslation::where('cms_blog_id', '<>', $id)
            ->where('url_key', $urlKey)
            ->limit(1)
            ->select(\DB::raw(1))
            ->exists();

        return $exists ? false : true;
    }

    /**
     * Retrive category from slug
     *
     * @param  string  $urlKey
     * @return \Fidem\CMS\Contracts\CmsBlog|\Exception
     */
    public function findByUrlKeyOrFail($urlKey)
    {
        $blog = $this->model->whereTranslation('url_key', $urlKey)->first();

        if ($blog) {
            return $blog;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $urlKey
        );
    }

    /**
     * @param  array  $data
     * @param  \Fidem\CMS\Contracts\CmsBlog  $blog
     * @return void
     */
    public function uploadImages($data, $blog, $type = "image")
    {
        if (isset($data[$type])) {
            $request = request();

            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'blog/' . $blog->id;

                if ($request->hasFile($file)) {
                    if ($blog->{$type}) {
                        Storage::delete($blog->{$type});
                    }

                    $blog->{$type} = $request->file($file)->store($dir);
                    $blog->save();
                }
            }
        } else {
            if ($blog->{$type}) {
                Storage::delete($blog->{$type});
            }

            $blog->{$type} = null;
            $blog->save();
        }
    }

    /**
     * Search Blog
     *
     * @param  string  $term
     * @return \Illuminate\Support\Collection
     */
    public function searchBlogByAttribute($term)
    {
        $results = app(CmsBlogRepository::class)->scopeQuery(function($query) use($term) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannel()->id ?: core()->getDefaultChannel()->id);

            $locale = request()->get('locale') ?: app()->getLocale();

            return $query->distinct()
                ->addSelect('cms_blogs.id', 'cms_blog_translations.blog_title')
                ->leftJoin('cms_blog_channels', function($leftJoin) use ($channel) {
                    $leftJoin->on('cms_blogs.id', '=', 'cms_blog_channels.cms_blog_id')
                        ->where('cms_blog_channels.channel_id', $channel);
                })
                ->leftJoin('cms_blog_translations', function($leftJoin) use ($locale) {
                    $leftJoin->on('cms_blogs.id', '=', 'cms_blog_translations.cms_blog_id')
                        ->whereNotNull('cms_blog_translations.url_key')
                        ->where('cms_blog_translations.locale', $locale);
                })
                ->where('cms_blog_translations.blog_title', 'like', '%' . urldecode($term) . '%')
                ->orderBy('cms_blogs.id', 'desc');
        })->paginate(16);

        return $results;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAll()
    {
        $params = request()->input();

        $results = app(CmsBlogRepository::class)->scopeQuery(function($query) use($params) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannel()->id ?: core()->getDefaultChannel()->id);

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('cms_blogs.*')
                ->leftJoin('cms_blog_channels', function($leftJoin) use ($channel) {
                    $leftJoin->on('cms_blogs.id', '=', 'cms_blog_channels.cms_blog_id')
                        ->where('cms_blog_channels.channel_id', $channel);
                })
                ->leftJoin('cms_blog_translations', function($leftJoin) use ($locale) {
                    $leftJoin->on('cms_blogs.id', '=', 'cms_blog_translations.cms_blog_id')
                        ->whereNotNull('cms_blog_translations.url_key')
                        ->where('cms_blog_translations.locale', $locale);
                });

            if (isset($params['search']))
                $qb->where('cms_blog_translations.blog_title', 'like', '%' . urldecode($params['search']) . '%');
var_dump($params['url_key']);die;
            if (isset($params['url_key']))
                $qb->where('cms_blog_translations.url_key', '=', $params['url_key']);

            return $qb->orderBy('cms_blogs.id', 'desc');
        })->paginate(isset($params['limit']) ? $params['limit'] : 9);

        return $results;
    }
}