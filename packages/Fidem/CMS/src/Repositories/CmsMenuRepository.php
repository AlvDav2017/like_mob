<?php

namespace Fidem\CMS\Repositories;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Webkul\Core\Eloquent\Repository;
use Fidem\CMS\Models\CmsMenuTranslation;

class CmsMenuRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Fidem\CMS\Contracts\CmsMenu';
    }

    /**
     * @param  array  $data
     * @return \Fidem\CMS\Contracts\CmsMenu
     */
    public function create(array $data)
    {
        Event::dispatch('cms.menus.create.before');

        $model = $this->getModel();

        foreach (core()->getAllLocales() as $locale) {
            foreach ($model->translatedAttributes as $attribute) {

                if (isset($data[$attribute])) {
                    $data[$locale->code][$attribute] = $data[$attribute];
                }
            }
        }

        $menu = parent::create($data);

        $menu->channels()->sync($data['channels']);

        Event::dispatch('cms.menus.create.after', $menu);

        return $menu;
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \Fidem\CMS\Contracts\CmsMenu
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $menu = $this->find($id);

        Event::dispatch('cms.menus.update.before', $id);

        parent::update($data, $id, $attribute);

        $menu->channels()->sync($data['channels']);

        Event::dispatch('cms.menus.update.after', $id);

        return $menu;
    }

    /**
     * Checks slug is unique or not based on locale
     *
     * @param  int  $id
     * @param  string  $urlKey
     * @return bool
     */
    public function isUrlKeyUnique($id, $urlKey)
    {
        $exists = CmsMenuTranslation::where('cms_menu_id', '<>', $id)
            ->where('url_key', $urlKey)
            ->limit(1)
            ->select(\DB::raw(1))
            ->exists();

        return $exists ? false : true;
    }

    /**
     * Retrive category from slug
     *
     * @param  string  $urlKey
     * @return \Fidem\CMS\Contracts\CmsMenu|\Exception
     */
    public function findByUrlKeyOrFail($urlKey)
    {
        $menu = $this->model->whereTranslation('url_key', $urlKey)->first();

        if ($menu) {
            return $menu;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $urlKey
        );
    }
}