<?php

return [
    [
        'key'        => 'cms.blogs',
        'name'       => 'cms::app.blogs.title',
        'route'      => 'admin.cms.blog.index',
        'sort'       => 2,
        'icon-class' => '',
    ],
    [
        'key'        => 'cms.comments',
        'name'       => 'cms::app.blogs.comments.title',
        'route'      => 'admin.cms.blog.comment.index',
        'sort'       => 3,
        'icon-class' => '',
    ],
    [
        'key'        => 'cms.menus',
        'name'       => 'cms::app.menus.title',
        'route'      => 'admin.cms.menu.index',
        'sort'       => 4,
        'icon-class' => '',
    ]
];