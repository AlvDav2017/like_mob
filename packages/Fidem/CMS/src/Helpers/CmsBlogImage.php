<?php

namespace Fidem\CMS\Helpers;

use Illuminate\Support\Facades\Storage;

class CmsBlogImage
{
    /**
     * Get product's base image
     *
     * @param  \Fidem\CMS\Contracts\CmsBlog $blog
     * @return array
     */
    public function getCmsBlogBaseImage($blog)
    {
        $image = $blog ? $blog->image : null;

        if ($image) {
            $image = url('storage/' . $image);
        } else {
            $image = asset('vendor/webkul/ui/assets/images/product/large-product-placeholder.png');
        }

        return $image;
    }
}