<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsMenuTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_menu_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->integer('page_id')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('url_key')->nullable();
            $table->integer('position')->default(0);
            $table->boolean('status')->default(0);
            $table->string('locale');

            $table->integer('cms_menu_id')->unsigned();
            $table->unique(['cms_menu_id', 'url_key', 'locale']);
            $table->foreign('cms_menu_id')->references('id')->on('cms_menus')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_menu_translations');
    }
}
