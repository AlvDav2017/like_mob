<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsBlogChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_blog_channels', function (Blueprint $table) {
            $table->integer('cms_blog_id')->unsigned();
            $table->integer('channel_id')->unsigned();

            $table->unique(['cms_blog_id', 'channel_id']);
          //  $table->foreign('cms_blog_id')->references('id')->on('cms_blogs')->onDelete('cascade');
         //   $table->foreign('channel_id')->references('id')->on('channels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_blog_channels');
    }
}
