<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsMenuChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_menu_channels', function (Blueprint $table) {
            $table->integer('cms_menu_id')->unsigned();
            $table->integer('channel_id')->unsigned();

            $table->unique(['cms_menu_id', 'channel_id']);
            $table->foreign('cms_menu_id')->references('id')->on('cms_menus')->onDelete('cascade');
            $table->foreign('channel_id')->references('id')->on('channels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_menu_channels');
    }
}
