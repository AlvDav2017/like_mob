<?php

namespace Fidem\CMS\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CMSBlogsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('cms_blogs')->delete();
        DB::table('cms_blog_translations')->delete();

        DB::table('cms_blogs')->insert([
            [
                'id'         => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);

        DB::table('cms_blog_translations')->insert([
            [
                'locale'           => 'en',
                'cms_blog_id'      => 1,
                'url_key'          => 'hello-world',
                'html_content'     => '<div class="static-container"><div class="mb-5">Hello world blog content</div></div>',
                'image'            => null,
                'blog_title'       => 'Hello world',
                'meta_title'       => 'hello world',
                'meta_description' => '',
                'meta_keywords'    => 'helloworld',
            ]
        ]);
    }
}