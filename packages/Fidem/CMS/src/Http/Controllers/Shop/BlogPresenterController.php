<?php

namespace Fidem\CMS\Http\Controllers\Shop;

use Fidem\CMS\Http\Controllers\Controller;
use Fidem\CMS\Repositories\CmsRepository;

class BlogPresenterController extends Controller
{
    /**
     * CmsRepository object
     *
     * @var \Webkul\CMS\Repositories\CmsRepository
     */
    protected $cmsRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\CMS\Repositories\CmsRepository  $cmsRepository
     * @return void
     */
    public function __construct(CmsRepository $cmsRepository)
    {
        $this->cmsRepository = $cmsRepository;
    }

    /**
     * To extract the blog content and load it in the respective view file
     *
     * @param  string  $urlKey
     * @return \Illuminate\View\View
     */
    public function presenter($urlKey)
    {
        $blog = $this->cmsRepository->findByUrlKeyOrFail($urlKey);

        return view('fidem_shop::cms.blog')->with('blog', $blog);
    }
}