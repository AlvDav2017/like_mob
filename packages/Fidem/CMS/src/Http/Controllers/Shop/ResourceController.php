<?php

namespace Fidem\CMS\Http\Controllers\Shop;

use Fidem\CMS\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResourceController extends Controller
{
    /**
     * Contains current guard
     *
     * @var array
     */
    protected $guard;
    
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Repository object
     *
     * @var \Webkul\Core\Eloquent\Repository
     */
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->guard = request()->has('token') ? 'api' : 'customer';

        $this->_config = request('_config');

        if (isset($this->_config['authorization_required']) && $this->_config['authorization_required']) {

            auth()->setDefaultDriver($this->guard);

            $this->middleware('auth:' . $this->guard);
        }

        $this->repository = app($this->_config['repository']);
    }

    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = $this->repository->scopeQuery(function($query) {
            foreach (request()->except(['page', 'limit', 'pagination', 'sort', 'order', 'token']) as $input => $value) {
                $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if ($sort = request()->input('sort')) {
                $query = $query->orderBy($sort, request()->input('order') ?? 'desc');
            } else {
                $query = $query->orderBy('id', 'desc');
            }

            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('limit') ?? 10);
        } else {
            $results = $query->get();
        }

        return $this->_config['resource']::collection($results);
    }

    /**
     * Returns a individual resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        return new $this->_config['resource'](
            $this->repository->findOrFail($id)
        );
    }

    /**
     * Delete's a individual resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        
        return response()->json([
            'message' => 'Item removed successfully.',
        ]);
    }

    /**
     * Returns top blog
     *
     * @return \Illuminate\Support\Collection
     */
    public function getTopBlogs()
    {
        $results = $this->repository->scopeQuery(function($query) {
            return $query->distinct()
                ->addSelect('cms_blogs.*')
                ->leftJoin('cms_blog_channels', function($leftJoin) {
                    $leftJoin->on('cms_blogs.id', '=', 'cms_blog_channels.cms_blog_id')
                        ->where('cms_blog_channels.channel_id', core()->getCurrentChannel()->id);
                })
                ->leftJoin('cms_blog_translations', function($leftJoin) {
                    $leftJoin->on('cms_blogs.id', '=', 'cms_blog_translations.cms_blog_id')
                        ->where('cms_blog_translations.locale', app()->getLocale());
                })
                ->where('cms_blog_translations.top', 1)
                ->orderBy('cms_blogs.id', 'desc');
        })->paginate(request()->input('limit') ?? 4);

        return $this->_config['resource']::collection($results);
    }

    /**
     * Returns a individual resource.
     *
     * @param  string  $urlKey
     * @return \Illuminate\Http\Response
     */
    public function getByUrlKeyOrFail($urlKey)
    {
        return new $this->_config['resource'](
            $this->repository->findByUrlKeyOrFail($urlKey)
        );
    }
}
