<?php

namespace Fidem\CMS\Http\Controllers\Shop;

use Fidem\CMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Fidem\CMS\Repositories\CmsBlogCommentRepository;
use Fidem\CMS\Http\Resources\CmsBlogComment as CmsBlogCommentResource;

class CommentController extends Controller
{
    /**
     * Contains current guard
     *
     * @var array
     */
    protected $guard;

    /**
     * ProductReviewRepository object
     *
     * @var \Fidem\CMS\Repositories\CmsBlogCommentRepository
     */
    protected $cmsBlogommentRepository;

    /**
     * Controller instance
     *
     * @param  Fidem\CMS\Repositories\CmsBlogCommentRepository  $cmsBlogommentRepository
     */
    public function __construct(CmsBlogCommentRepository $cmsBlogommentRepository)
    {
        $this->guard = request()->has('token') ? 'api' : 'customer';

        auth()->setDefaultDriver($this->guard);

        $this->cmsBlogommentRepository = $cmsBlogommentRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $customer = auth($this->guard)->user();
        
        $this->validate(request(), [
            'comment' => 'required'
        ]);

        $data = array_merge(request()->all(), [
            'customer_id' => $customer ? $customer->id : null,
            'name'        => $customer ? $customer->name : request()->input('name'),
            'status'      => 'pending',
            'cms_blog_id'  => $id,
        ]);

        $cmsBlogComment = $this->cmsBlogommentRepository->create($data);

        return response()->json([
            'message' => 'Your comment submitted successfully.',
            'data'    => new CmsBlogCommentResource($this->cmsBlogommentRepository->find($cmsBlogComment->id)),
        ]);
    }
}