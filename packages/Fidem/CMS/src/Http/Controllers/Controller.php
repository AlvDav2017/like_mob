<?php

namespace Fidem\CMS\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class Controller extends BaseController
{
    /**
     * @SWG\Info(
     *      version="1.0.0",
     *      title="OpenApi",
     *      description="OpenApi description",
     *      @SWG\Contact(
     *          email="arturkaramyanfidem@gmail.com"
     *      ),
     *     @SWG\License(
     *         name="Apache 2.0",
     *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *     )
     * )
     */
    use DispatchesJobs, ValidatesRequests;
}