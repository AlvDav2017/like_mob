<?php

namespace Fidem\CMS\Http\Controllers\Admin;

use Fidem\CMS\Http\Controllers\Controller;
use Fidem\CMS\Models\CmsMenu;
use Fidem\CMS\Repositories\CmsMenuRepository;
use Webkul\CMS\Models\CmsPage;

class MenuController extends Controller
{
    /**
     * To hold the request variables from route file
     * 
     * @var array
     */
    protected $_config;

    /**
     * To hold the CMSRepository instance
     * 
     * @var \Webkul\CMS\Repositories\CmsRepository
     */
    protected $cmsRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\CMS\Repositories\CmsRepository  $cmsRepository
     * @return void
     */
    public function __construct(CmsMenuRepository $cmsRepository)
    {
        $this->cmsRepository = $cmsRepository;

        $this->_config = request('_config');
    }

    /**
     * Loads the index menu showing the static menus resources
     * 
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * To create a new CMS menu
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $pages = CmsPage::all();
        $menus = CmsMenu::all();

        return view($this->_config['view'], compact('pages', 'menus'));
    }

    /**
     * To store a new CMS menu in storage
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'status'   => 'required',
            'type'     => 'required|in:header,footer',
            'name' => 'required',
            'url_key'      => ['required_with:custom_url,on', 'unique:cms_menu_translations,url_key', new \Webkul\Core\Contracts\Validations\Slug],
            'channels'     => 'required',
        ]);

        $menu = $this->cmsRepository->create(request()->all());

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'menu']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * To edit a previously created CMS menu
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = $this->cmsRepository->findOrFail($id);
        $pages = CmsPage::all();
        $menus = CmsMenu::all();

        return view($this->_config['view'], compact('item', 'pages', 'menus'));
    }

    /**
     * To update the previously created CMS menu in storage
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $locale = request()->get('locale') ?: app()->getLocale();

        request()->merge([
            $locale => array_merge([
                'parent_id' => request()->input($locale.'.parent_id', null),
                'page_id' => request()->input($locale.'.page_id', null)
            ], request()->input($locale))
        ]);

        $this->validate(request(), [
            $locale . '.type'   => 'required|in:header,footer',
            $locale . '.name' => 'required',
            $locale . '.url_key'      => ['required_with:custom_url,on', new \Webkul\Core\Contracts\Validations\Slug, function ($attribute, $value, $fail) use ($id) {
                if (! $this->cmsRepository->isUrlKeyUnique($id, $value)) {
                    $fail(trans('admin::app.response.already-taken', ['name' => 'menu']));
                }
            }],
            'channels' => 'required',
        ]);

        $this->cmsRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'menu']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * To delete the previously create CMS menu
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $menu = $this->cmsRepository->findOrFail($id);

        if ($menu->delete()) {
            session()->flash('success', trans('cms::app.menus.delete-success'));

            return response()->json(['message' => true], 200);
        } else {
            session()->flash('success', trans('cms::app.menus.delete-failure'));

            return response()->json(['message' => false], 200);
        }
    }

    /**
     * To mass delete the CMS resource from storage
     *
     * @return \Illuminate\Http\Response
     */
    public function massDelete()
    {
        $data = request()->all();

        if ($data['indexes']) {
            $menuIDs = explode(',', $data['indexes']);

            $count = 0;

            foreach ($menuIDs as $menuId) {
                $menu = $this->cmsRepository->find($menuId);

                if ($menu) {
                    $menu->delete();

                    $count++;
                }
            }

            if (count($menuIDs) == $count) {
                session()->flash('success', trans('admin::app.datagrid.mass-ops.delete-success', [
                    'resource' => 'CMS menus',
                ]));
            } else {
                session()->flash('success', trans('admin::app.datagrid.mass-ops.partial-action', [
                    'resource' => 'CMS menus',
                ]));
            }
        } else {
            session()->flash('warning', trans('admin::app.datagrid.mass-ops.no-resource'));
        }

        return redirect()->route('admin.cms.menu.index');
    }
}