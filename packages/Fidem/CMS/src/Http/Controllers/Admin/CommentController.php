<?php

namespace Fidem\CMS\Http\Controllers\Admin;

use Fidem\CMS\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
use Fidem\CMS\Repositories\CmsBlogCommentRepository;

class CommentController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ProductReviewRepository object
     *
     * @var \Fidem\CMS\Repositories\CmsBlogCommentRepository
     */
    protected $cmsBlogCommentRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Fidem\CMS\Repositories\CmsBlogCommentRepository  $cmsBlogCommentRepository
     * @return void
     */
    public function __construct(CmsBlogCommentRepository $cmsBlogCommentRepository)
    {
        $this->cmsBlogCommentRepository = $cmsBlogCommentRepository;

        $this->_config = request('_config');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
    */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $comment = $this->cmsBlogCommentRepository->findOrFail($id);

        return view($this->_config['view'], compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        Event::dispatch('customer.comment.update.before', $id);

        $this->cmsBlogCommentRepository->update(request()->all(), $id);

        Event::dispatch('customer.comment.update.after', $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Comment']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Delete the comment of the current blog
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsBlogComment = $this->cmsBlogCommentRepository->findOrFail($id);

        try {
            Event::dispatch('customer.comment.delete.before', $id);

            $this->cmsBlogCommentRepository->delete($id);

            Event::dispatch('customer.comment.delete.after', $id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Comment']));

            return response()->json(['message' => true], 200);
        } catch (\Exception $e) {
            report($e);
            session()->flash('success', trans('admin::app.response.delete-failed', ['name' => 'Comment']));
        }

        return response()->json(['message' => false], 400);
    }

    /**
     * Mass delete the comments on the blogs.
     *
     * @return \Illuminate\Http\Response
     */
    public function massDestroy()
    {
        $suppressFlash = false;

        if (request()->isMethod('post')) {
            $data = request()->all();

            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                try {
                    Event::dispatch('customer.comment.delete.before', $value);

                    $this->cmsBlogCommentRepository->delete($value);

                    Event::dispatch('customer.comment.delete.after', $value);
                } catch(\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash) {
                session()->flash('success', trans('admin::app.datagrid.mass-ops.delete-success', ['resource' => 'Comments']));
            } else {
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'Comments']));
            }

            return redirect()->route($this->_config['redirect']);

        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }

    /**
     * Mass approve the comments on the blogs.
     *
     * @return \Illuminate\Http\Response
     */
    public function massUpdate()
    {
        $suppressFlash = false;

        if (request()->isMethod('post')) {
            $data = request()->all();

            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                $review = $this->cmsBlogCommentRepository->findOneByField('id', $value);

                try {
                    if ($data['massaction-type'] == 'update') {
                        if ($data['update-options'] == 1) {
                            Event::dispatch('customer.comment.update.before', $value);

                            $review->update(['status' => 'approved']);

                            Event::dispatch('customer.comment.update.after', $review);
                        } elseif ($data['update-options'] == 0) {
                            $review->update(['status' => 'pending']);
                        } elseif ($data['update-options'] == 2) {
                            $review->update(['status' => 'disapproved']);
                        } else {
                            continue;
                        }
                    }
                } catch(\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash) {
                session()->flash('success', trans('admin::app.datagrid.mass-ops.update-success', ['resource' => 'Comments']));
            } else {
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'Comments']));
            }

            return redirect()->route($this->_config['redirect']);
        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }
}
