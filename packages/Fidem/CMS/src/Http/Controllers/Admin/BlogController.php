<?php

namespace Fidem\CMS\Http\Controllers\Admin;

use Fidem\CMS\Http\Controllers\Controller;
use Fidem\CMS\Repositories\CmsBlogRepository;

 class BlogController extends Controller
{
    /**
     * To hold the request variables from route file
     * 
     * @var array
     */
    protected $_config;

    /**
     * To hold the cmsBlogRepository instance
     * 
     * @var \Webkul\CMS\Repositories\CmsBlogRepository
     */
    protected $cmsBlogRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\CMS\Repositories\CmsBlogRepository  $cmsBlogRepository
     * @return void
     */
    public function __construct(CmsBlogRepository $cmsBlogRepository)
    {
        $this->cmsBlogRepository = $cmsBlogRepository;

        $this->_config = request('_config');
    }

    /**
     * Loads the index blog showing the static blogs resources
     * 
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * To create a new CMS blog
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $blog = $this->cmsBlogRepository->getModel();

        return view($this->_config['view'], compact('blog'));
    }

    /**
     * To store a new CMS blog in storage
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = request()->all();

        $this->validate(request(), [
            'url_key'      => ['required', 'unique:cms_blog_translations,url_key', new \Webkul\Core\Contracts\Validations\Slug],
            'blog_title'   => 'required',
            'channels'     => 'required',
            'html_content' => 'required',
            'image.*'     => 'mimes:jpeg,jpg,bmp,png'
        ]);

        $blog = $this->cmsBlogRepository->create(request()->all());

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'blog']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * To edit a previously created CMS blog
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $blog = $this->cmsBlogRepository->findOrFail($id);

        return view($this->_config['view'], compact('blog'));
    }

    /**
     * To update the previously created CMS blog in storage
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $locale = request()->get('locale') ?: app()->getLocale();

        $this->validate(request(), [
            $locale . '.url_key'      => ['required', new \Webkul\Core\Contracts\Validations\Slug, function ($attribute, $value, $fail) use ($id) {
                if (! $this->cmsBlogRepository->isUrlKeyUnique($id, $value)) {
                    $fail(trans('admin::app.response.already-taken', ['name' => 'blog']));
                }
            }],
            $locale . '.blog_title'   => 'required',
            $locale . '.html_content' => 'required',
            'channels'                => 'required',
            'image.*'         => 'mimes:jpeg,jpg,bmp,png',
        ]);

        $this->cmsBlogRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'blog']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * To delete the previously create CMS blog
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $blog = $this->cmsBlogRepository->findOrFail($id);

        if ($blog->delete()) {
            session()->flash('success', trans('admin::app.cms.blogs.delete-success'));

            return response()->json(['message' => true], 200);
        } else {
            session()->flash('success', trans('admin::app.cms.blogs.delete-failure'));

            return response()->json(['message' => false], 200);
        }
    }

    /**
     * To mass delete the CMS resource from storage
     *
     * @return \Illuminate\Http\Response
     */
    public function massDelete()
    {
        $data = request()->all();

        if ($data['indexes']) {
            $blogIDs = explode(',', $data['indexes']);

            $count = 0;

            foreach ($blogIDs as $blogId) {
                $blog = $this->cmsBlogRepository->find($blogId);

                if ($blog) {
                    $blog->delete();

                    $count++;
                }
            }

            if (count($blogIDs) == $count) {
                session()->flash('success', trans('admin::app.datagrid.mass-ops.delete-success', [
                    'resource' => 'CMS blogs',
                ]));
            } else {
                session()->flash('success', trans('admin::app.datagrid.mass-ops.partial-action', [
                    'resource' => 'CMS blogs',
                ]));
            }
        } else {
            session()->flash('warning', trans('admin::app.datagrid.mass-ops.no-resource'));
        }

        return redirect()->route('admin.cms.blog.index');
    }

     /**
      * Result of search blog.
      *
      * @return \Illuminate\View\View|\Illuminate\Http\Response
      */
     public function blogLinkSearch()
     {
         if (request()->ajax()) {
             $results = [];

             foreach ($this->cmsBlogRepository->searchBlogByAttribute(request()->input('query')) as $row) {
                 $results[] = [
                     'id'   => $row->id,
                     'blog_title' => $row->blog_title,
                 ];
             }

             return response()->json($results);
         } else {
             $blog = $this->cmsBlogRepository->getModel();

             return view($this->_config['view'], compact('blog'));
         }
     }

     public function upload($name)
     {
         $imageFolder = storage_path('app/public/'.$name);

         reset ($_FILES);
         $temp = current($_FILES);

         if (is_uploaded_file($temp['tmp_name'])){
             // Sanitize input
             if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                 header("HTTP/1.1 400 Invalid file name.");
                 return;
             }

             // Verify extension
             if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
                 header("HTTP/1.1 400 Invalid extension.");
                 return;
             }

             // Accept upload if there was no origin, or if it is an accepted origin
             $filetowrite = $imageFolder . $temp['name'];
             if(!is_dir($imageFolder))
                 mkdir($imageFolder, 0777, true);
             move_uploaded_file($temp['tmp_name'], $filetowrite);

             // Respond to the successful upload with JSON.
             // Use a location key to specify the path to the saved image resource.
             // { location : '/your/uploaded/image/file'}
             echo json_encode(array('location' => $temp['name']));
         } else {
             // Notify editor that the upload failed
             header("HTTP/1.1 500 Server Error");
         }
     }
}