<?php

namespace Fidem\CMS\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Webkul\Product\Helpers\ProductType;

class CmsBlog extends JsonResource
{
    /**
     * Create a new resource instance.
     *
     * @return void
     */
    public function __construct($resource)
    {
        $this->cmsBlogImageHelper = app('Fidem\CMS\Helpers\CmsBlogImage');

        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $related_blogs = [];

        foreach ($this->related_blogs as $related_blog) {
            $related_blogs[] = [
                'id'                     => $related_blog->id,
                'title'                  => $related_blog->blog_title,
                'url_key'                => $related_blog->url_key,
                'html_content'           => $related_blog->html_content,
                'base_image'             => $this->cmsBlogImageHelper->getCmsBlogBaseImage($related_blog),
                'top'                    => $related_blog->top ? true : false,
                'created_at'             => $related_blog->created_at,
                'updated_at'             => $related_blog->updated_at,
            ];
        }

        return [
            'id'                     => $this->id,
            'title'                  => $this->blog_title,
            'url_key'                => $this->url_key,
            'html_content'           => $this->html_content,
            'image'                  => $this->cmsBlogImageHelper->getCmsBlogBaseImage($this),
            'top'                    => $this->top ? true : false,
            'related_blogs'          => $related_blogs,
            'comments'               => CmsBlogComment::collection($this->comments),
            'created_at'             => $this->created_at,
            'updated_at'             => $this->updated_at,
        ];
    }
}