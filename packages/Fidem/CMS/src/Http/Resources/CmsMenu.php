<?php

namespace Fidem\CMS\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CmsMenu extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'type'             => $this->type,
            'page_id'          => $this->page_id,
            'parent_id'        => $this->parent_id,
            'url_key'          => $this->url_key,
            'position'         => $this->position,
            'status'           => $this->status,
            'created_at'       => $this->created_at,
            'updated_at'       => $this->updated_at,
        ];
    }
}