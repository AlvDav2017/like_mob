<?php

namespace Fidem\CMS\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CmsPage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'url_key'           => $this->url_key,
            'page_title'        => $this->page_title,
            'html_content'      => $this->html_content,
            'meta_description'  => $this->meta_description,
            'meta_title'        => $this->meta_title,
            'meta_keywords'     => $this->meta_keywords,
        ];
    }
}