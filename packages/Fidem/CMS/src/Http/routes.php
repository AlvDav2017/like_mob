<?php

Route::group(['middleware' => ['web']], function () {
    // Admin Routes
    Route::prefix('admin')->group(function () {
        Route::group(['namespace' => 'Fidem\CMS\Http\Controllers\Admin', 'middleware' => ['admin']], function () {
            Route::get('tinymce-upload/{name}', 'BlogController@upload')->name('admin.upload');
            /*
             * Cms
             */
            Route::prefix('cms')->group(function () {
                /*
                 * Blog
                 */
                Route::prefix('blog')->group(function () {
                    Route::get('/', 'BlogController@index')->defaults('_config', [
                        'view' => 'cms::blog.index'
                    ])->name('admin.cms.blog.index');

                    Route::get('create', 'BlogController@create')->defaults('_config', [
                        'view' => 'cms::blog.create'
                    ])->name('admin.cms.blog.create');

                    Route::post('create', 'BlogController@store')->defaults('_config', [
                        'redirect' => 'admin.cms.blog.index'
                    ])->name('admin.cms.blog.store');

                    Route::get('edit/{id}', 'BlogController@edit')->defaults('_config', [
                        'view' => 'cms::blog.edit'
                    ])->name('admin.cms.blog.edit');

                    Route::post('edit/{id}', 'BlogController@update')->defaults('_config', [
                        'redirect' => 'admin.cms.blog.index'
                    ])->name('admin.cms.blog.update');

                    Route::post('/delete/{id}', 'BlogController@delete')->defaults('_config', [
                        'redirect' => 'admin.cms.blog.index'
                    ])->name('admin.cms.blog.delete');

                    Route::post('/massdelete', 'BlogController@massDelete')->defaults('_config', [
                        'redirect' => 'admin.cms.blog.index'
                    ])->name('admin.cms.blog.mass-delete');

                    //blog search for linked bloks
                    Route::get('search', 'BlogController@blogLinkSearch')->defaults('_config', [
                        'view' => 'cms::blog.edit'
                    ])->name('admin.cms.blog.bloglinksearch');
                });
                /*
                 * Blog Comment
                 */
                Route::get('comments', 'CommentController@index')->defaults('_config',[
                    'view' => 'cms::blog.comment.index'
                ])->name('admin.cms.blog.comment.index');

                Route::get('comments/edit/{id}', 'CommentController@edit')->defaults('_config',[
                    'view' => 'cms::blog.comment.edit'
                ])->name('admin.cms.blog.comment.edit');

                Route::put('comments/edit/{id}', 'CommentController@update')->defaults('_config', [
                    'redirect' => 'admin.cms.blog.comment.index'
                ])->name('admin.cms.blog.comment.update');

                Route::post('comments/delete/{id}', 'CommentController@destroy')->defaults('_config', [
                    'redirect' => 'admin.cms.blog.comment.index'
                ])->name('admin.cms.blog.comment.delete');

                //mass destroy
                Route::post('comments/massdestroy', 'CommentController@massDestroy')->defaults('_config', [
                    'redirect' => 'admin.cms.blog.comment.index'
                ])->name('admin.cms.blog.comment.massdelete');

                //mass update
                Route::post('comments/massupdate', 'CommentController@massUpdate')->defaults('_config', [
                    'redirect' => 'admin.cms.blog.comment.index'
                ])->name('admin.cms.blog.comment.massupdate');
                /*
                 * Menu
                 */
                Route::prefix('menu')->group(function () {
                    Route::get('/', 'MenuController@index')->defaults('_config', [
                        'view' => 'cms::menu.index'
                    ])->name('admin.cms.menu.index');

                    Route::get('create', 'MenuController@create')->defaults('_config', [
                        'view' => 'cms::menu.create'
                    ])->name('admin.cms.menu.create');

                    Route::post('create', 'MenuController@store')->defaults('_config', [
                        'redirect' => 'admin.cms.menu.index'
                    ])->name('admin.cms.menu.store');

                    Route::get('edit/{id}', 'MenuController@edit')->defaults('_config', [
                        'view' => 'cms::menu.edit'
                    ])->name('admin.cms.menu.edit');

                    Route::post('edit/{id}', 'MenuController@update')->defaults('_config', [
                        'redirect' => 'admin.cms.menu.index'
                    ])->name('admin.cms.menu.update');

                    Route::post('/delete/{id}', 'MenuController@delete')->defaults('_config', [
                        'redirect' => 'admin.cms.menu.index'
                    ])->name('admin.cms.menu.delete');

                    Route::post('/massdelete', 'MenuController@massDelete')->defaults('_config', [
                        'redirect' => 'admin.cms.menu.index'
                    ])->name('admin.cms.menu.mass-delete');
                });
            });
        });
    });
});

Route::group(['prefix' => 'api'], function ($router) {

    // API Routes
    Route::group(['namespace' => 'Fidem\CMS\Http\Controllers\Shop', 'middleware' => ['locale', 'theme', 'currency']], function ($router) {

        //CMS routes
        Route::group(['prefix' => 'cms'], function ($router) {

            //Page routes
            Route::get('pages', 'ResourceController@index')->defaults('_config', [
                'repository'    => 'Webkul\CMS\Repositories\CmsRepository',
                'resource'      => 'Fidem\CMS\Http\Resources\CmsPage'
            ]);
            Route::get('page/{slug}', 'ResourceController@getByUrlKeyOrFail')->defaults('_config', [
                'repository'    => 'Webkul\CMS\Repositories\CmsRepository',
                'resource'      => 'Fidem\CMS\Http\Resources\CmsPage'
            ]);

            //Blog routes
            Route::get('blogs', 'ResourceController@index')->defaults('_config', [
                'repository'    => 'Fidem\CMS\Repositories\CmsBlogRepository',
                'resource'      => 'Fidem\CMS\Http\Resources\CmsBlog'
            ]);
            Route::get('blog/{slug}', 'ResourceController@getByUrlKeyOrFail')->defaults('_config', [
                'repository'    => 'Fidem\CMS\Repositories\CmsBlogRepository',
                'resource'      => 'Fidem\CMS\Http\Resources\CmsBlog'
            ]);
            
            Route::get('blogs/top', 'ResourceController@getTopBlogs')->defaults('_config', [
                'repository'    => 'Fidem\CMS\Repositories\CmsBlogRepository',
                'resource'      => 'Fidem\CMS\Http\Resources\CmsBlog'
            ]);

            //Blog Comment routes
            Route::get('comments', 'ResourceController@index')->defaults('_config', [
                'repository' => 'Fidem\CMS\Repositories\CmsBlogCommentRepository',
                'resource' => 'Fidem\CMS\Http\Resources\CmsBlogComment'
            ]);

            Route::get('comments/{id}', 'ResourceController@get')->defaults('_config', [
                'repository' => 'Fidem\CMS\Repositories\CmsBlogCommentRepository',
                'resource' => 'Fidem\CMS\Http\Resources\CmsBlogComment'
            ]);

            Route::post('comments/{id}/create', 'CommentController@store');

            Route::delete('comments/{id}', 'ResourceController@destroy')->defaults('_config', [
                'repository' => 'Fidem\CMS\Repositories\CmsBlogCommentRepository',
                'resource' => 'Fidem\CMS\Http\Resources\CmsBlogComment',
                'authorization_required' => true
            ]);

            //Menu routes
            Route::get('menus', 'ResourceController@index')->defaults('_config', [
                'repository'    => 'Fidem\CMS\Repositories\CmsMenuRepository',
                'resource'      => 'Fidem\CMS\Http\Resources\CmsMenu'
            ]);
        });
    });
});