<?php

namespace Fidem\CMS\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class CMSMenuDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('cms_menus')
            ->select('cms_menus.id', 'cms_menu_translations.name', 'cms_menu_translations.status')
            ->leftJoin('cms_menu_channels', function($leftJoin) {
                $leftJoin->on('cms_menus.id', '=', 'cms_menu_channels.cms_menu_id')
                    ->where('cms_menu_channels.channel_id', core()->getCurrentChannel()->id);
            })
            ->leftJoin('cms_menu_translations', function($leftJoin) {
                $leftJoin->on('cms_menus.id', '=', 'cms_menu_translations.cms_menu_id')
                         ->where('cms_menu_translations.locale', app()->getLocale());
            });

        $this->addFilter('id', 'cms_menus.id');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'id',
            'label'      => trans('admin::app.datagrid.id'),
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'name',
            'label'      => trans('cms::app.menus.name'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'status',
            'label'      => trans('admin::app.datagrid.status'),
            'type'       => 'boolean',
            'sortable'   => true,
            'searchable' => true,
            'filterable' => true,
            'wrapper'    => function($value) {
                if ($value->status == 1) {
                    return trans('admin::app.datagrid.active');
                } else {
                    return trans('admin::app.datagrid.inactive');
                }
            },
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'title'  => trans('admin::app.datagrid.edit'),
            'method' => 'GET',
            'route'  => 'admin.cms.menu.edit',
            'icon'   => 'icon pencil-lg-icon',
        ]);

        $this->addAction([
            'title'  => trans('admin::app.datagrid.delete'),
            'method' => 'POST',
            'route'  => 'admin.cms.menu.delete',
            'icon'   => 'icon trash-icon',
        ]);
    }

    public function prepareMassActions()
    {
        $this->addMassAction([
            'type'   => 'delete',
            'label'  => trans('admin::app.datagrid.delete'),
            'action' => route('admin.cms.menu.mass-delete'),
            'method' => 'DELETE',
        ]);
    }
}