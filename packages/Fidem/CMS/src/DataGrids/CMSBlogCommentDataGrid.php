<?php

namespace Fidem\CMS\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class CMSBlogCommentDataGrid extends DataGrid
{
    protected $index = 'blog_comment_id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {        $queryBuilder = DB::table('cms_blog_comments as bc')
            ->leftJoin('cms_blog_translations as bl', function($leftJoin) {
                $leftJoin->on('bc.cms_blog_id', '=', 'bl.cms_blog_id')
                    ->where('bl.locale', app()->getLocale());
            })
            ->leftJoin('cms_blog_channels', function($leftJoin) {
                $leftJoin->on('bc.cms_blog_id', '=', 'cms_blog_channels.cms_blog_id')
                    ->where('cms_blog_channels.channel_id', core()->getCurrentChannel()->id);
            })
            ->select('bc.id as blog_comment_id', 'bc.comment', 'bc.name as blog_name', 'bc.status as blog_comment_status');

        $this->addFilter('blog_comment_id', 'bc.id');
        $this->addFilter('blog_comment_status', 'bc.status');
        $this->addFilter('blog_name', 'bc.name');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'blog_comment_id',
            'label'      => trans('admin::app.datagrid.id'),
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'comment',
            'label'      => trans('admin::app.datagrid.comment'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'blog_name',
            'label'      => trans('cms::app.blogs.datagrid.blog-name'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => false,
        ]);

        $this->addColumn([
            'index'      => 'blog_comment_status',
            'label'      => trans('admin::app.datagrid.status'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'width'      => '100px',
            'filterable' => true,
            'closure'    => true,
            'wrapper'    => function ($value) {
                if ($value->blog_comment_status == 'approved') {
                    return '<span class="badge badge-md badge-success">' . trans('admin::app.datagrid.approved') . '</span>';
                } elseif ($value->blog_comment_status == "pending") {
                    return '<span class="badge badge-md badge-warning">' . trans('admin::app.datagrid.pending') . '</span>';
                } elseif ($value->blog_comment_status == "disapproved") {
                    return '<span class="badge badge-md badge-danger">' . trans('admin::app.datagrid.disapproved') . '</span>';
                }
            },
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'title'  => trans('admin::app.datagrid.edit'),
            'method' => 'GET',
            'route'  => 'admin.cms.blog.comment.edit',
            'icon'   => 'icon pencil-lg-icon',
        ]);

        $this->addAction([
            'title'  => trans('admin::app.datagrid.delete'),
            'method' => 'POST',
            'route'  => 'admin.cms.blog.comment.delete',
            'icon'   => 'icon trash-icon',
        ]);
    }

    public function prepareMassActions()
    {
        $this->addMassAction([
            'type'  => 'delete',
            'label'  => trans('admin::app.datagrid.delete'),
            'action' => route('admin.cms.blog.comment.massdelete'),
            'method' => 'DELETE',
        ]);

        $this->addMassAction([
            'type'    => 'update',
            'label'   => trans('admin::app.datagrid.update-status'),
            'action'  => route('admin.cms.blog.comment.massupdate'),
            'method'  => 'PUT',
            'options' => [
                trans('admin::app.customers.reviews.pending')     => 0,
                trans('admin::app.customers.reviews.approved')    => 1,
                trans('admin::app.customers.reviews.disapproved') => 2,
            ],
        ]);
    }
}