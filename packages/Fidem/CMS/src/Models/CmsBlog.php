<?php

namespace Fidem\CMS\Models;

use Webkul\Core\Eloquent\TranslatableModel;
use Illuminate\Support\Facades\Storage;
use Fidem\CMS\Contracts\CmsBlog as CmsBlogContract;
use Webkul\Core\Models\ChannelProxy;

class CmsBlog extends TranslatableModel implements CmsBlogContract
{
    protected $fillable = ['layout'];

    public $translatedAttributes = [
        'content',
        'meta_description',
        'meta_title',
        'blog_title',
        'meta_keywords',
        'html_content',
        'top',
        'url_key',
    ];

    protected $with = ['translations'];

    /**
     * Get image url for the blog image.
     */
    public function image_url()
    {
        if (! $this->image)
            return;

        return Storage::url($this->image);
    }

    /**
     * Get image url for the blog image.
     */
    public function getImageUrlAttribute()
    {
        return $this->image_url();
    }

    /**
     * Get the channels.
     */
    public function channels()
    {
        return $this->belongsToMany(ChannelProxy::modelClass(), 'cms_blog_channels');
    }

    /**
     * The related cms blogs that belong to the cms blog.
     */
    public function related_blogs()
    {
        return $this->belongsToMany(static::class, 'cms_blog_relations', 'parent_id', 'child_id')->limit(4);
    }

    /**
     * Get the blog comments that owns the blog.
     */
    public function comments()
    {
        return $this->hasMany(CmsBlogCommentProxy::modelClass());
    }
}