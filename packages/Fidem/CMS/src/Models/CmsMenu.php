<?php

namespace Fidem\CMS\Models;

use Webkul\Core\Eloquent\TranslatableModel;
use Fidem\CMS\Contracts\CmsMenu as CmsMenuContract;
use Webkul\Core\Models\ChannelProxy;

class CmsMenu extends TranslatableModel implements CmsMenuContract
{
    protected $fillable = ['layout'];

    public $translatedAttributes = [
        'name',
        'type',
        'page_id',
        'parent_id',
        'url_key',
        'position',
        'status',
    ];

    protected $with = ['translations'];

    /**
     * Get the channels.
     */
    public function channels()
    {
        return $this->belongsToMany(ChannelProxy::modelClass(), 'cms_menu_channels');
    }
}