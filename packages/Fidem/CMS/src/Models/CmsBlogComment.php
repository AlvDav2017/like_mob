<?php

namespace Fidem\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Customer\Models\CustomerProxy;
use Fidem\CMS\Models\CmsBlog;
use Fidem\CMS\Contracts\CmsBlogComment as CmsBlogCommentContract;

class CmsBlogComment extends Model implements CmsBlogCommentContract
{
    protected $fillable = [
        'comment',
        'status',
        'cms_blog_id',
        'customer_id',
        'name',
    ];

    /**
     * Get the blog attribute family that owns the blog.
     */
    public function customer()
    {
        return $this->belongsTo(CustomerProxy::modelClass());
    }

    /**
     * Get the blog.
     */
    public function cms_blog()
    {
        return $this->belongsTo(CmsBlogProxy::modelClass());
    }
}