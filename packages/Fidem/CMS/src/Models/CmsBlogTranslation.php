<?php

namespace Fidem\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use Fidem\CMS\Contracts\CmsBlogTranslation as CmsBlogTranslationContract;

class CmsBlogTranslation extends Model implements CmsBlogTranslationContract
{
    public $timestamps = false;

    protected $fillable = [
        'blog_title',
        'url_key',
        'html_content',
        'top',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'locale',
        'cms_blog_id',
    ];
}