<?php

namespace Fidem\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use Fidem\CMS\Contracts\CmsMenuTranslation as CmsMenuTranslationContract;

class CmsMenuTranslation extends Model implements CmsMenuTranslationContract
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'type',
        'page_id',
        'parent_id',
        'url_key',
        'position',
        'status',
        'locale',
        'cms_menu_id',
    ];

    protected $with = ['children'];

    /**
     * Get the children for the menu.
     */
    public function children()
    {
        return $this->hasMany(CmsMenuTranslationProxy::modelClass(), 'parent_id', 'id')->with('children')->orderBy('position');
    }
}