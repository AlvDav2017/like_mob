<?php

return [
    'blogs' => [
        'title' => 'Blogs',
        'top' => 'Top',
        'add-title' => 'Add Blog',
        'create-btn-title' => 'Save Blog',
        'edit-title' => 'Edit Blog',
        'edit-btn-title' => 'Save Blog',
        'blog-title' => 'Blog Title',
        'delete-success' => 'CMS blog deleted successfully',
        'delete-failure' => 'CMS blog cannot be deleted',
        'blog-link' => 'Linked Blogs',
        'related-blogs' => 'Related Blogs',
        'blog-search-hint' => 'Start typing blog name',
        'no-result-found' => 'Blogs not found with same name.',
        'datagrid' => [
            'blog-name' => 'Blog'
        ],
        'comments' => [
            'title' => 'Comments',
            'edit-title' => 'Edit Comment'
        ]
    ],
    'menus' => [
        'title' => 'Menus',
        'add-title' => 'Add Menu',
        'status' => 'Status',
        'type' => 'Type',
        'header' => 'Header',
        'footer' => 'Footer',
        'name' => 'Name',
        'position' => 'Position',
        'add-custom-url' => 'Add Custom Url',
        'page' => 'Page',
        'parent' => 'Parent',
        'create-btn-title' => 'Save Menu',
        'edit-title' => 'Edit Menu',
        'edit-btn-title' => 'Save Menu',
        'delete-success' => 'CMS menu deleted successfully',
        'delete-failure' => 'CMS menu cannot be deleted',
    ]
];
