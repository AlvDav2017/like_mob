@extends('admin::layouts.content')

@section('page_title')
    {{ __('cms::app.blogs.title') }}
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('cms::app.blogs.title') }}</h1>
            </div>
            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span >
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>

                <a href="{{ route('admin.cms.blog.create') }}" class="btn btn-lg btn-primary">
                    {{ __('cms::app.blogs.add-title') }}
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('cmsGrid', 'Fidem\CMS\DataGrids\CMSBlogDataGrid')

            {!! $cmsGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>
@stop

@push('scripts')
    @include('admin::export.export', ['gridName' => $cmsGrid])
@endpush

