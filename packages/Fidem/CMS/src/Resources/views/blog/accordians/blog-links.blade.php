<accordian :title="'{{ __('cms::app.blogs.blog-link') }}'" :active="false">
    <div slot="body">

        <linked-blogs></linked-blogs>

    </div>
</accordian>

@push('scripts')

<script type="text/x-template" id="linked-blogs-template">
    <div>

        <div class="control-group" v-for='(key) in linkedBlogs'>
            <label for="related_blogs" v-if="(key == 'related_blogs')">
                {{ __('cms::app.blogs.related-blogs') }}
            </label>

            <input type="text" class="control" autocomplete="off" v-model="search_term[key]" placeholder="{{ __('cms::app.blogs.blog-search-hint') }}" v-on:keyup="search(key)">

            <div class="linked-product-search-result">
                <ul>
                    <li v-for='(blog, index) in blogs[key]' v-if='blogs[key].length' @click="addBlog(blog, key)">
                        @{{ blog.blog_title }}
                    </li>

                    <li v-if='! blogs[key].length && search_term[key].length && ! is_searching[key]'>
                        {{ __('cms::app.blogs.no-result-found') }}
                    </li>

                    <li v-if="is_searching[key] && search_term[key].length">
                        {{ __('admin::app.catalog.products.searching') }}
                    </li>
                </ul>
            </div>

            <input type="hidden" name="related_blogs[]" v-for='(blog, index) in addedBlogs.related_blogs' v-if="(key == 'related_blogs') && addedBlogs.related_blogs.length" :value="blog.id"/>

            <span class="filter-tag" style="text-transform: capitalize; margin-top: 10px; margin-right: 0px; justify-content: flex-start" v-if="addedBlogs[key].length">
                <span class="wrapper" style="margin-left: 0px; margin-right: 10px;" v-for='(blog, index) in addedBlogs[key]'>
                    @{{ blog.blog_title }}
                <span class="icon cross-icon" @click="removeBlog(blog, key)"></span>
                </span>
            </span>
        </div>

    </div>
</script>

<script>

    Vue.component('linked-blogs', {

        template: '#linked-blogs-template',

        data: function() {
            return {
                blogs: {
                    'related_blogs': []
                },

                search_term: {
                    'related_blogs': ''
                },

                addedBlogs: {
                    'related_blogs': []
                },

                is_searching: {
                    'related_blogs': false
                },

                blogId: {{ $blog->id ?? 0 }},

                linkedBlogs: ['related_blogs'],

                relatedBlogs: @json($blog->related_blogs()->get()),
            }
        },

        created: function () {
            if (this.relatedBlogs.length >= 1) {
                for (var index in this.relatedBlogs) {
                    this.addedBlogs.related_blogs.push(this.relatedBlogs[index]);
                }
            }
        },

        methods: {
            addBlog: function (blog, key) {
                this.addedBlogs[key].push(blog);
                this.search_term[key] = '';
                this.blogs[key] = []
            },

            removeBlog: function (blog, key) {
                for (var index in this.addedBlogs[key]) {
                    if (this.addedBlogs[key][index].id == blog.id ) {
                        this.addedBlogs[key].splice(index, 1);
                    }
                }
            },

            search: function (key) {
                this_this = this;

                this.is_searching[key] = true;

                if (this.search_term[key].length >= 1) {
                    this.$http.get ("{{ route('admin.cms.blog.bloglinksearch') }}", {params: {query: this.search_term[key]}})
                        .then (function(response) {

                            for (var index in response.data) {
                                if (response.data[index].id == this_this.blogId) {
                                    response.data.splice(index, 1);
                                }
                            }

                            if (this_this.addedBlogs[key].length) {
                                for (var blog in this_this.addedBlogs[key]) {
                                    for (var blogId in response.data) {
                                        if (response.data[blogId].id == this_this.addedBlogs[key][blog].id) {
                                            response.data.splice(blogId, 1);
                                        }
                                    }
                                }
                            }
                            
                            this_this.blogs[key] = response.data;

                            this_this.is_searching[key] = false;
                        })

                        .catch (function (error) {
                            this_this.is_searching[key] = false;
                        })
                } else {
                    this_this.blogs[key] = [];
                    this_this.is_searching[key] = false;
                }
            }
        }
    });

</script>

@endpush