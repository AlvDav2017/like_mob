@foreach($children as $menu)
    <div class="tree-item active">
        <i class="expand-icon"></i>
        <span class="radio">
            <input type="radio" id="{{ $menu->id }}" name="{{ isset($locale) ? $locale.'[parent_id]' : 'parent_id' }}" value="{{ $menu->id }}">
            <label for="{{ $menu->id }}" class="radio-view"></label>
            <span for="{{ $menu->id }}">{{ $menu->name }}</span>
        </span>

        @if($menu->children->count())
            <div class="tree-item has-children active">
                @include('cms::menu.children', ['children'=> $menu->children])
            </div>
        @endif
    </div>
@endforeach