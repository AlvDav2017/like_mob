@extends('admin::layouts.content')

@section('page_title')
    {{ __('cms::app.menus.edit-title') }}
@stop

@section('content')
    <div class="content">

        <?php $locale = request()->get('locale') ?: app()->getLocale(); ?>

        <form method="POST" id="page-form" action="" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('cms::app.menus.edit-title') }}
                    </h1>

                    <div class="control-group">
                        <select class="control" id="locale-switcher" onChange="window.location.href = this.value">
                            @foreach (core()->getAllLocales() as $localeModel)

                                <option value="{{ route('admin.cms.menu.edit', $item->id) . '?locale=' . $localeModel->code }}" {{ ($localeModel->code) == $locale ? 'selected' : '' }}>
                                    {{ $localeModel->name }}
                                </option>

                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('cms::app.menus.edit-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">

                <div class="form-container">
                    @csrf()

                    {!! view_render_event('bagisto.admin.menus.create_form_accordian.general.before') !!}

                    <accordian :title="'{{ __('admin::app.cms.pages.general') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group">
                                <label for="status" class="required">{{ __('cms::app.menus.status') }}</label>
                                <label class="switch">
                                    <input type="checkbox" id="status" name="{{$locale}}[status]" value="1" {{ old('status', $item->status) ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>

                                <span class="control-error" v-if="errors.has('status')">@{{ errors.first('status') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('type') ? 'has-error' : '']">
                                <label for="type" class="required">{{ __('cms::app.menus.type') }}</label>
                                <select class="control" v-validate="'required'" id="type" name="{{$locale}}[type]" data-vv-as="&quot;{{ __('cms::app.menus.type') }}&quot;">
                                    <option value="header" {{ $item->type === 'header' ? 'selected' : '' }}>
                                        {{ __('cms::app.menus.header') }}
                                    </option>
                                    <option value="footer" {{ $item->type === 'footer' ? 'selected' : '' }}>
                                        {{ __('cms::app.menus.footer') }}
                                    </option>
                                </select>

                                <span class="control-error" v-if="errors.has('type')">@{{ errors.first('type') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">{{ __('cms::app.menus.name') }}</label>
                                <input type="text" class="control" name="{{$locale}}[name]" v-validate="'required'" value="{{ old('name', $item->name) }}" data-vv-as="&quot;{{ __('cms::app.menus.name') }}&quot;">

                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('position') ? 'has-error' : '']">
                                <label for="position">{{ __('cms::app.menus.position') }}</label>
                                <input type="text" v-validate="'numeric'" class="control" id="position" name="{{$locale}}[position]" value="{{ old('position', $item->position) }}" data-vv-as="&quot;{{ __('cms::app.menus.position') }}&quot;"/>
                                <span class="control-error" v-if="errors.has('position')">@{{ errors.first('position') }}</span>
                            </div>

                            <div class="control-group">
                                <label for="status">{{ __('cms::app.menus.add-custom-url') }}</label>

                                <label class="switch">
                                    <input type="checkbox" name="custom_url">
                                    <span class="slider round"></span>
                                </label>
                            </div>

                            <div class="control-group @if(! old($locale.'.url_key', $item->url_key)) hide @endif" :class="[errors.has('url-key') ? 'has-error' : '']">
                                <label for="url-key">{{ __('admin::app.cms.pages.url-key') }}</label>
                                <input type="text" class="control" name="{{$locale}}[url_key]" value="{{ old('url_key', $item->url_key) }}" data-vv-as="&quot;{{ __('admin::app.cms.pages.url-key') }}&quot;" @if(! old($locale.'.url_key', $item->url_key)) disabled="disabled" @endif>

                                <span class="control-error" v-if="errors.has('url_key')">@{{ errors.first('url_key') }}</span>
                            </div>

                            <div class="control-group @if(old($locale.'.url_key', $item->url_key)) hide @endif" :class="[errors.has('page_id') ? 'has-error' : '']">
                                <label for="page_id">{{ __('cms::app.menus.page') }}</label>
                                <select class="control" id="page_id" name="{{$locale}}[page_id]" data-vv-as="&quot;{{ __('cms::app.menus.page') }}&quot;">
                                    @foreach($pages as $page)
                                        @if($page = $page->translate($locale))
                                            <option value="{{ $page->cms_page_id }}" @if($page->cms_page_id == $item->page_id) selected @endif>
                                                {{  $page->page_title  }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>

                                <span class="control-error" v-if="errors.has('page_id')">@{{ errors.first('page_id') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('parent_id') ? 'has-error' : '']">
                                <label for="parent_id">{{ __('cms::app.menus.parent') }}</label>
                                <div class="tree-container">
                                    @foreach($menus as $menu)
                                        @if($menu = $menu->translate($locale))
                                            <div class="tree-item active">
                                                <i class="expand-icon"></i>
                                                <span class="radio">
                                                    <input type="radio" id="{{$menu->id}}" name="{{$locale}}[parent_id]" value="{{$menu->id}}">
                                                    <label for="{{$menu->id}}" class="radio-view"></label>
                                                    <span for="{{$menu->id}}">{{$menu->name}}</span>
                                                </span>

                                                @if($menu->children->count())
                                                    <div class="tree-item has-children active">
                                                        @include('cms::menu.children', ['children'=> $menu->children, 'locale' => $locale])
                                                    </div>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                                <span class="control-error" v-if="errors.has('parent_id')">@{{ errors.first('parent_id') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('channels[]') ? 'has-error' : '']">
                                <label for="channels" class="required">{{ __('admin::app.cms.pages.channel') }}</label>

                                <?php $selectedOptionIds = old('inventory_sources') ?: $item->channels->pluck('id')->toArray() ?>

                                <select type="text" class="control" name="channels[]" v-validate="'required'" value="{{ old('channel[]') }}" data-vv-as="&quot;{{ __('admin::app.cms.pages.channel') }}&quot;" multiple="multiple">
                                    @foreach(app('Webkul\Core\Repositories\ChannelRepository')->all() as $channel)
                                        <option value="{{ $channel->id }}" {{ in_array($channel->id, $selectedOptionIds) ? 'selected' : '' }}>
                                            {{ $channel->name }}
                                        </option>
                                    @endforeach
                                </select>

                                <span class="control-error" v-if="errors.has('channels[]')">@{{ errors.first('channels[]') }}</span>
                            </div>
                        </div>
                    </accordian>

                    {!! view_render_event('bagisto.admin.menus.create_form_accordian.general.after') !!}
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')

    <script>
        $( document ).on('change', 'input[type=checkbox][name="custom_url"]', function () {
            let urlKey = $('input[name="{{$locale}}[url_key]"]'),
                page = $('select[name="{{$locale}}[page_id]"]');

            if (urlKey.closest('.control-group').hasClass('hide')) {
                urlKey.prop("disabled", false);
                urlKey.closest('.control-group').removeClass('hide');
                page.prop("disabled", true);
                page.closest('.control-group').addClass('hide');
            } else {
                page.prop("disabled", false);
                page.closest('.control-group').removeClass('hide');
                urlKey.prop("disabled", true);
                urlKey.closest('.control-group').addClass('hide');
            }
        });
    </script>

@endpush