<?php

namespace Fidem\CMS\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Fidem\CMS\Models\CmsBlog::class,
        \Fidem\CMS\Models\CmsBlogTranslation::class,
        \Fidem\CMS\Models\CmsBlogComment::class,
        \Fidem\CMS\Models\CmsMenu::class,
        \Fidem\CMS\Models\CmsMenuTranslation::class
    ];
}