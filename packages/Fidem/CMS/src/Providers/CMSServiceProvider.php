<?php

namespace Fidem\CMS\Providers;

use Illuminate\Support\ServiceProvider;
use Fidem\CMS\Models\CmsBlogProxy;
use Fidem\CMS\Observers\CmsBlogObserver;

class CMSServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../Http/routes.php');

        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'cms');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'cms');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        CmsBlogProxy::observe(CmsBlogObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfig();
    }

    /**
     * Register package config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/menu.php', 'menu.admin'
        );
    }
}