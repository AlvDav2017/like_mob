<?php

namespace Fidem\Translation\Http\Controllers\Admin;

use Fidem\Translation\Http\Controllers\Controller;
use Fidem\Translation\Repositories\TranslationRepository;

 class TranslationController extends Controller
{
    /**
     * To hold the request variables from route file
     * 
     * @var array
     */
    protected $_config;

    /**
     * To hold the CMSRepository instance
     * 
     * @var \Webkul\CMS\Repositories\CmsRepository
     */
    protected $translationRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Fidem\Translation\Repositories\TranslationRepository  $translationRepository
     * @return void
     */
    public function __construct(TranslationRepository $translationRepository)
    {
        $this->middleware('admin');

        $this->translationRepository = $translationRepository;

        $this->_config = request('_config');
    }

    /**
     * Loads the index blog showing the static blogs resources
     * 
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * To create a new CMS blog
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * To store a new CMS blog in storage
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = request()->all();

        $this->validate(request(), [
            'key'       => 'required|unique:translation_translations,key',
            'channels'  => 'required',
            'value'     => 'required'
        ]);

        $translation = $this->translationRepository->create(request()->all());

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'translation']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * To edit a previously created translation
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $translation = $this->translationRepository->findOrFail($id);

        return view($this->_config['view'], compact('translation'));
    }

    /**
     * To update the previously created translation in storage
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $locale = request()->get('locale') ?: app()->getLocale();

        $this->validate(request(), [
            $locale . '.key'      => ['required', function ($attribute, $value, $fail) use ($id) {
                if (! $this->translationRepository->isKeyUnique($id, $value)) {
                    $fail(trans('admin::app.response.already-taken', ['name' => 'translation']));
                }
            }],
            $locale . '.value' => 'required',
            'channels'         => 'required'
        ]);

        $this->translationRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'translation']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * To delete the previously create translation
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $translation = $this->translationRepository->findOrFail($id);

        if ($translation->delete()) {
            session()->flash('success', trans('admin::app.translations.delete-success'));

            return response()->json(['message' => true], 200);
        } else {
            session()->flash('success', trans('admin::app.translations.delete-failure'));

            return response()->json(['message' => false], 200);
        }
    }

    /**
     * To mass delete the translation resource from storage
     *
     * @return \Illuminate\Http\Response
     */
    public function massDelete()
    {
        $data = request()->all();

        if ($data['indexes']) {
            $translationIDs = explode(',', $data['indexes']);

            $count = 0;

            foreach ($translationIDs as $translationId) {
                $translation = $this->translationRepository->find($translationId);

                if ($translation) {
                    $translation->delete();

                    $count++;
                }
            }

            if (count($translationIDs) == $count) {
                session()->flash('success', trans('admin::app.datagrid.mass-ops.delete-success', [
                    'resource' => 'Translations',
                ]));
            } else {
                session()->flash('success', trans('admin::app.datagrid.mass-ops.partial-action', [
                    'resource' => 'Translations',
                ]));
            }
        } else {
            session()->flash('warning', trans('admin::app.datagrid.mass-ops.no-resource'));
        }

        return redirect()->route('admin.translations.index');
    }
}