<?php

Route::group(['middleware' => ['web']], function () {
    // Admin Routes
    Route::prefix('admin')->group(function () {
        Route::group(['namespace' => 'Fidem\Translation\Http\Controllers\Admin', 'middleware' => ['admin']], function () {
            //Translation routes
            Route::prefix('translations')->group(function () {
                Route::get('/', 'TranslationController@index')->defaults('_config', [
                    'view' => 'translations::translations.index'
                ])->name('admin.translations.index');

                Route::get('create', 'TranslationController@create')->defaults('_config', [
                    'view' => 'translations::translations.create'
                ])->name('admin.translation.create');

                Route::post('create', 'TranslationController@store')->defaults('_config', [
                    'redirect' => 'admin.translations.index'
                ])->name('admin.translation.store');

                Route::get('edit/{id}', 'TranslationController@edit')->defaults('_config', [
                    'view' => 'translations::translations.edit'
                ])->name('admin.translation.edit');

                Route::post('edit/{id}', 'TranslationController@update')->defaults('_config', [
                    'redirect' => 'admin.translations.index'
                ])->name('admin.translation.update');

                Route::post('/delete/{id}', 'TranslationController@delete')->defaults('_config', [
                    'redirect' => 'admin.translations.index'
                ])->name('admin.translation.delete');

                Route::post('/massdelete', 'TranslationController@massDelete')->defaults('_config', [
                    'redirect' => 'admin.translation.index'
                ])->name('admin.translation.mass-delete');
            });
        });
    });
});

Route::group(['prefix' => 'api'], function ($router) {
    // API Routes
    Route::group(['namespace' => 'Fidem\Translation\Http\Controllers\Shop', 'middleware' => ['locale', 'theme', 'currency']], function ($router) {

        //Translation routes
        Route::get('translations', 'ResourceController@index')->defaults('_config', [
            'repository'    => 'Fidem\Translation\Repositories\TranslationRepository'
        ]);
    });
});