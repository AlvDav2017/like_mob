<?php

namespace Fidem\Translation\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Fidem\Translation\Models\Translation::class,
        \Fidem\Translation\Models\TranslationTranslation::class
    ];
}