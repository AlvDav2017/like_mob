<?php

namespace Fidem\Translation\Models;

use Illuminate\Database\Eloquent\Model;
use Fidem\Translation\Contracts\TranslationTranslation as TranslationTranslationContract;

class TranslationTranslation extends Model implements TranslationTranslationContract
{
    public $timestamps = false;

    protected $fillable = [
        'key',
        'value',
        'locale',
        'translation_id',
    ];
}