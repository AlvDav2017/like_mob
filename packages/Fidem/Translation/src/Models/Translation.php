<?php

namespace Fidem\Translation\Models;

use Webkul\Core\Eloquent\TranslatableModel;
use Illuminate\Support\Facades\Storage;
use Fidem\Translation\Contracts\Translation as TranslationContract;
use Webkul\Core\Models\ChannelProxy;

class Translation extends TranslatableModel implements TranslationContract
{
    protected $fillable = ['layout'];

    public $translatedAttributes = [
        'value',
        'key',
    ];

    protected $with = ['translations'];

    /**
     * Get the channels.
     */
    public function channels()
    {
        return $this->belongsToMany(ChannelProxy::modelClass(), 'translation_channels');
    }
}