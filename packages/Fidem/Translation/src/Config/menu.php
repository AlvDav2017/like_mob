<?php

return [
    [
        'key'        => 'translations',
        'name'       => 'translations::app.translations.title',
        'route'      => 'admin.translations.index',
        'sort'       => 10,
        'icon-class' => 'folder-icon',
    ],
    [
        'key'        => 'translations.translations',
        'name'       => 'translations::app.translations.title',
        'route'      => 'admin.translations.index',
        'sort'       => 1,
        'icon-class' => '',
    ]
];