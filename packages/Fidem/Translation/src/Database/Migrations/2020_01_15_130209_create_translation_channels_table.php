<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation_channels', function (Blueprint $table) {
            $table->integer('translation_id')->unsigned();
            $table->integer('channel_id')->unsigned();

            $table->unique(['translation_id', 'channel_id']);
            $table->foreign('translation_id')->references('id')->on('translations')->onDelete('cascade');
            $table->foreign('channel_id')->references('id')->on('channels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translation_channels');
    }
}
