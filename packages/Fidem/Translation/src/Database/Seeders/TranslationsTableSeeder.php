<?php

namespace Fidem\Translation\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TranslationsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('translations')->delete();
        DB::table('translation_translations')->delete();

        DB::table('translations')->insert([
            [
                'id'         => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);

        DB::table('translation_translations')->insert([
            [
                'locale'            => 'en',
                'translation_id'    => 1,
                'key'               => 'hello-world',
                'value'             => 'Hello world blog content'
            ]
        ]);
    }
}