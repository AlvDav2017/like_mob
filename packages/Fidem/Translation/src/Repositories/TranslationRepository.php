<?php

namespace Fidem\Translation\Repositories;

use Illuminate\Support\Facades\Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Webkul\Core\Eloquent\Repository;
use Fidem\Translation\Models\TranslationTranslation;

class TranslationRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Fidem\Translation\Contracts\Translation';
    }

    /**
     * @param  array  $data
     * @return \Fidem\Translation\Contracts\Translation
     */
    public function create(array $data)
    {
        Event::dispatch('translations.create.before');

        $model = $this->getModel();

        foreach (core()->getAllLocales() as $locale) {
            foreach ($model->translatedAttributes as $attribute) {

                if (isset($data[$attribute])) {
                    $data[$locale->code][$attribute] = $data[$attribute];
                }
            }
        }

        $translation = parent::create($data);

        $translation->channels()->sync($data['channels']);

        Event::dispatch('translations.create.after', $translation);

        return $translation;
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \Fidem\Translation\Contracts\Translation
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $translation = $this->find($id);

        Event::dispatch('translations.update.before', $id);

        parent::update($data, $id, $attribute);

        $translation->channels()->sync($data['channels']);

        Event::dispatch('translations.update.after', $id);

        return $translation;
    }

    /**
     * Checks slug is unique or not based on locale
     *
     * @param  int  $id
     * @param  string  $key
     * @return bool
     */
    public function isKeyUnique($id, $key)
    {
        $exists = TranslationTranslation::where('translation_id', '<>', $id)
            ->where('key', $key)
            ->limit(1)
            ->select(\DB::raw(1))
            ->exists();

        return $exists ? false : true;
    }

    /**
     * Retrive category from slug
     *
     * @param  string  $key
     * @return \Fidem\Translation\Contracts\Translation|\Exception
     */
    public function findByKeyOrFail($key)
    {
        $translation = $this->model->whereTranslation('key', $key)->first();

        if ($translation) {
            return $translation;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $key
        );
    }

    /**
     * get translation tree
     *
     * @return \Illuminate\Support\Collection
     */
    public function getAll()
    {
        $data = [];

        collect(
            \Fidem\Translation\Models\TranslationTranslation::select('locale', 'key', 'value')->get()
        )->each(function ($item) use (&$data) {

            if(! isset($data[$item['locale']])) {
                $data[$item['locale']]['translations'] = [];
            }
            $data[$item['locale']]['translations'] = array_merge($data[$item['locale']]['translations'], [
                $item['key'] => $item['value']
            ]);
        });

        return $data;
    }
}