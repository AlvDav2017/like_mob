@extends('admin::layouts.content')

@section('page_title')
    {{ __('translations::app.translations.edit-title') }}
@stop

@section('content')
    <div class="content">
        <?php $locale = request()->get('locale') ?: app()->getLocale(); ?>

        <form method="POST" id="page-form" action="" @submit.prevent="onSubmit">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('translations::app.translations.edit-title') }}
                    </h1>

                    <div class="control-group">
                        <select class="control" id="locale-switcher" onChange="window.location.href = this.value">
                            @foreach (core()->getAllLocales() as $localeModel)

                                <option value="{{ route('admin.translation.edit', $translation->id) . '?locale=' . $localeModel->code }}" {{ ($localeModel->code) == $locale ? 'selected' : '' }}>
                                    {{ $localeModel->name }}
                                </option>

                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.cms.pages.edit-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">

                <div class="form-container">
                    @csrf()
                    <accordian :title="'{{ __('admin::app.cms.pages.general') }}'" :active="true">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('channels[]') ? 'has-error' : '']">
                                <label for="url-key" class="required">{{ __('admin::app.cms.pages.channel') }}</label>

                                <?php $selectedOptionIds = old('inventory_sources') ?: $translation->channels->pluck('id')->toArray() ?>

                                <select type="text" class="control" name="channels[]" v-validate="'required'" value="{{ old('channel[]') }}" data-vv-as="&quot;{{ __('admin::app.cms.pages.channel') }}&quot;" multiple="multiple">
                                    @foreach(app('Webkul\Core\Repositories\ChannelRepository')->all() as $channel)
                                        <option value="{{ $channel->id }}" {{ in_array($channel->id, $selectedOptionIds) ? 'selected' : '' }}>
                                            {{ $channel->name }}
                                        </option>
                                    @endforeach
                                </select>

                                <span class="control-error" v-if="errors.has('channels[]')">@{{ errors.first('channels[]') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('{{$locale}}[key]') ? 'has-error' : '']">
                                <label for="key" class="required">{{ __('translations::app.translations.key') }}</label>

                                <input type="text" class="control" name="{{$locale}}[key]" v-validate="'required'" value="{{ old($locale)['key'] ?? ($translation->translate($locale)['key'] ?? '') }}" data-vv-as="&quot;{{ __('translations::app.translations.key') }}&quot;" readonly>

                                <span class="control-error" v-if="errors.has('{{$locale}}[key]')">@{{ errors.first('{!!$locale!!}[key]') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('{{$locale}}[value]') ? 'has-error' : '']">
                                <label for="value" class="required">{{ __('translations::app.translations.value') }}</label>

                                <textarea type="text" class="control" id="value" name="{{$locale}}[value]" v-validate="'required'" data-vv-as="&quot;{{ __('translation::app.translations.value') }}&quot;">{{ old($locale)['value'] ?? ($translation->translate($locale)['value'] ?? '') }}</textarea>

                                <span class="control-error" v-if="errors.has('{{$locale}}[value]')">@{{ errors.first('{!!$locale!!}[value]') }}</span>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop