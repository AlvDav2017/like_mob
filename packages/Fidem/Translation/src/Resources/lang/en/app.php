<?php

return [
    'translations' => [
        'title' => 'Translations',
        'add-title' => 'Add Translation',
        'value' => 'Value',
        'key' => 'Key',
        'create-btn-title' => 'Save Translation',
        'edit-title' => 'Edit Translation',
        'edit-btn-title' => 'Save Translation',
        'translation-title' => 'Translation Title'
    ]
];
