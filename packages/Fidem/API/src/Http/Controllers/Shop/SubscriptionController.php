<?php
namespace  Fidem\API\Http\Controllers\Shop;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Webkul\Shop\Mail\SubscriptionEmail;
use Webkul\Core\Repositories\SubscribersListRepository;
use Webkul\Shop\Http\Controllers\Controller;


class SubscriptionController extends Controller{

    /**
     * SubscribersListRepository
     *
     * @var \Webkul\Core\Repositories\SubscribersListRepository
     */
    protected $subscriptionRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Core\Repositories\SubscribersListRepository  $subscriptionRepository
     * @return void
     */
    public function __construct(SubscribersListRepository $subscriptionRepository)
    {
        $this->subscriptionRepository = $subscriptionRepository;

        parent::__construct();
    }


    /**
     * Subscribes email to the email subscription list
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function index(Request $request)
    {

        $this->validate(request(), [
            'subscriber_email' => 'email|required',
        ]);

        $email = request()->input('subscriber_email');

        $unique = 0;

        $alreadySubscribed = $this->subscriptionRepository->findWhere(['email' => $email]);




        $unique = function () use ($alreadySubscribed) {
            return $alreadySubscribed->count() > 0 ? 0 : 1;
        };

        if ($unique()) {
            $token = uniqid();

            $subscriptionData['email'] = $email;
            $subscriptionData['token'] = $token;

            $mailSent = true;

            try {
                Mail::queue(new SubscriptionEmail($subscriptionData));

               // session()->flash('success', trans('shop::app.subscription.subscribed'));
            } catch (\Exception $e) {
                report($e);

               // session()->flash('error', trans('shop::app.subscription.not-subscribed'));

                $mailSent = false;
            }

            $result = false;

            if ($mailSent) {

                $result = $this->subscriptionRepository->create([
                    'email'         => $email,
                    'channel_id'    => core()->getCurrentChannel()->id,
                    'is_subscribed' => 1,
                    'token'         => $token,
                ]);

                if (! $result) {
                    // session()->flash('error', trans('shop::app.subscription.not-subscribed'));
                    // return redirect()->back();
                    return response()->json(['error'=>"not-subscribed"],502);

                }
            }
        } else {
            return response()->json(['error'=>"already"],403);
            // session()->flash('error', trans('shop::app.subscription.already'));
        }

        return response()->json(['success'=>'subscribed'],200);
    }



    public function unsubscribe($token)
    {
        $subscriber = $this->subscriptionRepository->findOneByField('token', $token);

        if (isset($subscriber)) {
            if ($subscriber->count() > 0 && $subscriber->is_subscribed == 1 && $subscriber->update(['is_subscribed' => 0])) {
                session()->flash('info', trans('shop::app.subscription.unsubscribed'));
            } else {
                session()->flash('info', trans('shop::app.subscription.already-unsub'));
            }
        }

        return redirect()->route('shop.home.index');
    }



}