<?php

namespace Fidem\API\Http\Controllers\Shop;


use \Illuminate\Http\Request;
use Webkul\Payment\Facades\Payment;


class PaymentController extends Controller
{
    public function getPayments()
    {

        $payments = Payment::getPaymentMethods();
        $data = [];
        foreach ($payments as $pay) {

            $new = [
                'method' => $pay['method'],
                'title' => core()->getConfigData('sales.paymentmethods.' . $pay['method'] . '.title', '', request()->input('locale')),
                'description' => core()->getConfigData('sales.paymentmethods.' . $pay['method'] . '.description', '', request()->input('locale')),
                'sort' => core()->getConfigData('sales.paymentmethods.' . $pay['method'] . '.sort', '', request()->input('locale')),

            ];

            array_push($data, $new);
        }


        return $data;

    }


}