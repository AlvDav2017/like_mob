<?php

namespace Fidem\API\Http\Controllers\Shop;

use Illuminate\Support\Facades\Mail;
use Fidem\API\Mail\ContactUsNotification;

class ContactUsController extends Controller
{
    /**
     * Returns a individual resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function send()
    {
        $data = request()->all();

        $this->validate(request(), [
            'full_name' => 'required',
            'email'     => 'required|email',
            'subject'   => 'required',
            'message'   => 'required',
        ]);

        try {
            Mail::queue(new ContactUsNotification($data));

            return response()->json([
                    'success' => true
                ]
            );
        } catch (\Exception $e) {
            report($e);
        }
    }
}