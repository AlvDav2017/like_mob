<?php

namespace Fidem\API\Http\Controllers\Shop;

use Webkul\Core\Http\Controllers\Controller;
use Webkul\Core\Repositories\SliderRepository;
class SliderController extends Controller
{


    /**
     * SliderRepository
     *
     * @var \Webkul\Core\Repositories\SliderRepository
     */
    protected $sliderRepository;

    /**
     * @var array
     */
    protected $channels;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Core\Repositories\SliderRepository  $sliderRepository
     * @return void
     */
    public function __construct(SliderRepository $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;

    }


    public function getSliders(){


        $locale= \request()->get('locale');

        return $this->sliderRepository->where('locale',$locale)->get();

    }


}