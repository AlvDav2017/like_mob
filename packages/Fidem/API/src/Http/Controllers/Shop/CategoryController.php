<?php

namespace Fidem\API\Http\Controllers\Shop;

use Webkul\Category\Repositories\CategoryRepository;

class CategoryController extends Controller
{

    /**
     * Repository object
     *
     * @var \Webkul\Category\Repositories\CategoryRepository
     */
    protected $categoryRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Category\Repositories\CategoryRepository  $categoryRepository
     * @return void
     */
    public function __construct(
        CategoryRepository $categoryRepository
    )   {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return array
     */
    public function fetchCategories()
    {
        $formattedCategories = [];
        $categories = $this->categoryRepository->getVisibleCategoryTree();


         ;
        foreach ($categories as $category) {
            array_push($formattedCategories, $this->getCategoryFilteredData($category));
        }

        return [
            'status'     => true,
            'categories' => $formattedCategories,
        ];
    }

    /**
     * @param  \Webkul\Category\Contracts\Category  $category
     * @return array
     */
    private function getCategoryFilteredData($category)
    {
        $formattedChildCategory = [];

        foreach ($category->children as $child) {
            array_push($formattedChildCategory, $this->getCategoryFilteredData($child));
        }

         return [
            'id'                 => $category->id,
            'slug'               => $category->slug,
            'name'               => $category->name,
            'children'           => $formattedChildCategory,
            'category_icon_path' => $category->category_icon_path ? 'https://prod.haysell.com/images/upload/category/thumb/450/480/' . $category->category_icon_path : asset('vendor/webkul/ui/assets/images/product/large-product-placeholder.png'),
            'image_url'          => $category->image ? 'https://prod.haysell.com/images/upload/category/thumb/450/480/' . $category->image : asset('vendor/webkul/ui/assets/images/product/large-product-placeholder.png'),
        ];
    }
}