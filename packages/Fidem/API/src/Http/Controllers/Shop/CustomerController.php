<?php

namespace Fidem\API\Http\Controllers\Shop;

use Illuminate\Support\Facades\Validator;
use Webkul\Customer\Repositories\CustomerRepository;
use Webkul\Customer\Repositories\CustomerGroupRepository;
use Webkul\Customer\Repositories\CustomerAddressRepository;
use Illuminate\Support\Facades\Event;


class CustomerController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Repository object
     *
     * @var \Webkul\Customer\Repositories\CustomerRepository
     */
    protected $customerRepository;

    /**
     * Repository object
     *
     * @var \Webkul\Customer\Repositories\CustomerGroupRepository
     */
    protected $customerGroupRepository;

    /**
     * CustomerAddressRepository object
     *
     * @var \Webkul\Customer\Repositories\CustomerAddressRepository
     */
    protected $customerAddressRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Customer\Repositories\CustomerRepository  $customerRepository
     * @param  \Webkul\Customer\Repositories\CustomerGroupRepository  $customerGroupRepository
     * @return void
     */
    public function __construct(
        CustomerRepository $customerRepository,
        CustomerGroupRepository $customerGroupRepository,
        CustomerAddressRepository $customerAddressRepository
    )   {
        $this->_config = request('_config');

        $this->customerRepository = $customerRepository;

        $this->customerGroupRepository = $customerGroupRepository;

        $this->customerAddressRepository = $customerAddressRepository;
    }

    /**
     * Method to store user's sign up form data to DB.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

         $validation = Validator::make(request()->all(),[

            'email'      => 'email|required|unique:customers,email',
            'password'   => 'required|min:6'
        ]);



        if ($validation->fails()) {

            return response()->json( $validation->errors());
        }else {

//            request()->validate([
//                'first_name' => 'required',
//                'last_name' => 'required',
//                'email' => 'email|required|unique:customers,email',
//                'password' => 'confirmed|min:6|required',
//            ]);

            $data = request()->input();

            $data = array_merge($data, [
                'password' => bcrypt($data['password']),
                'channel_id' => core()->getCurrentChannel()->id,
                'is_verified' => 1,
            ]);

            $data['customer_group_id'] = $this->customerGroupRepository->findOneWhere(['code' => 'general'])->id;

            Event::dispatch('customer.registration.before');

            $customer = $this->customerRepository->create($data);

            Event::dispatch('customer.registration.after', $customer);

            return response()->json([
                'message' => 'Your account has been created successfully.',
            ]);
        }
    }
}