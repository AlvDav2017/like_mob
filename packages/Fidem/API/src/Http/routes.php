<?php

Route::group(['prefix' => 'api'], function ($router) {
    Route::group(['namespace' => 'Fidem\API\Http\Controllers\Shop', 'middleware' => ['locale', 'theme', 'currency']], function ($router) {
        //Customer routes
        Route::post('customer/register', 'CustomerController@create');

        //Contact Us routes
        Route::post('contact-us-send', 'ContactUsController@send');

        Route::get('categories', 'CategoryController@fetchCategories');

        Route::get('sliders','SliderController@getSliders');
    });



    Route::group(['namespace' => 'Fidem\Product\Http\Controllers\Shop', 'middleware' => ['locale', 'theme', 'currency']], function ($router) {
        //Product routes
        Route::get('products', 'ProductController@index');
    });

    Route::get('payments', 'Fidem\API\Http\Controllers\Shop\PaymentController@getPayments');

    Route::post('subscribe', 'Fidem\API\Http\Controllers\Shop\SubscriptionController@index');

    Route::get('unsubscribe/{token}', 'Webkul\Shop\Http\Controllers\SubscriptionController@unsubscribe')->name('shop.unsubscribe');


});