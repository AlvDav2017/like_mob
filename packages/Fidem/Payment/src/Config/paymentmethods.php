<?php
return [
    'ameria_vpos' => [
        'code'             => 'ameria_vpos',
        'title'            => 'Credit card',
        'description'      => 'Pay securely using your credit card.',
        'class'            => 'Fidem\AmeriaVpos\Payment\Vpos',
        'sandbox'          => true,
        'active'           => true,
        'client_id'        => '',
        'username'         => '',
        'password'         => '',
        'sort'             => 4,
    ]
];