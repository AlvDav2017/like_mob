<?php

namespace Fidem\Payment\Providers;

use Illuminate\Support\ServiceProvider;
use Webkul\Checkout\Models\CartProxy;
use Fidem\Payment\Observers\CartObserver;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        CartProxy::observe(CartObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfig();
    }
    /**
     * Register package config.
     *
     * @return void
     */
    protected function registerConfig()
    {


        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/paymentmethods.php', 'paymentmethods'
        );

        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/system.php', 'core'
        );
    }
}
