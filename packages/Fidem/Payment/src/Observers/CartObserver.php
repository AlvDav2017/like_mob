<?php

namespace Fidem\Payment\Observers;

use Fidem\CartApiToken\Models\CartApiToken;

class CartObserver
{
    /**
     * Handle the Cart "update" event.
     *
     * @param  \Webkul\Checkout\Contracts\Cart $cart
     * @return void
     */
    public function updated($cart)
    {
        if( ! $cart->is_active && request()->has('api_token') ) {
            $api_token = CartApiToken::where('api_token', request()->input('api_token'))->first();

            if( ! is_null($api_token->cart_id) ) {
                $api_token->cart_id = null;
                $api_token->save();
            }
        }
    }
}